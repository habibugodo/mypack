package tz.co.nbc.mypack.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
@Data
public class AppConfig {

    @Value("${app.jwtSecret}")
    private String JWT_SECRET;

    @Value("${app.jwtExpirationInMinutes}")
    private int JWT_EXPIRATION;

    @Value("${app.sender}")
    private String SENDER;

    @Value("${app.refreshTokenExpirationInMinutes}")
    private Long REFRESH_TOKEN_EXPIRATION;

    @Value("${app.passwordResetTokenExpirationInMinutes}")
    private Long PASSWORD_RESET_TOKEN_EXPIRATION;

    @Value("${app.fileUploadDir}")
    private String UPLOAD_DIR;

    @Value("${app.downloadUrl}")
    private String DOWNLOAD_URL;

//    @Value("${app.cbsApiURL}")
//    private String CBS_API_URL="https://nacbe-v001-nbc-alternative-channels.apps.ocp4-kawe.tz.af.absa.local";

    public static String CBS_API_URL="https://internal-gateway.apps.ocp4.tz.af.absa.local:443/nacbev001-vendorsacopening/1.0.0";

    private String DEFAULT_PAGE_NUMBER = "0";

    private String DEFAULT_PAGE_SIZE = "30";
}