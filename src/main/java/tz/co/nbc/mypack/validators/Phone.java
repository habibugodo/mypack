package tz.co.nbc.mypack.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PhoneValidator.class)
public @interface Phone {

    public String message() default "Invalid Phone Number: Mobile number should be valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}