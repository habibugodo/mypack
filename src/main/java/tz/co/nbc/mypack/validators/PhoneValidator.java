package tz.co.nbc.mypack.validators;



import tz.co.nbc.mypack.utils.PhoneUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, String> {
    @Override
    public boolean isValid(String phone, ConstraintValidatorContext constraintValidatorContext) {
        if(phone.length()==10||phone.length()==12){
            PhoneUtils.getMobileNumberWithCodeNoPlus(phone);
            return true;
        }else{
            return false;
        }
    }
}