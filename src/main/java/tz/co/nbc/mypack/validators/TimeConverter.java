package tz.co.nbc.mypack.validators;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class TimeConverter {

    public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return LocalDateTime.ofInstant(
                dateToConvert.toInstant(), ZoneId.systemDefault());
    }
}
