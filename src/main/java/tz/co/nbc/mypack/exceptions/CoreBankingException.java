package tz.co.nbc.mypack.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CoreBankingException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public CoreBankingException(String message) {
        super(message);
    }

    public CoreBankingException(String message, Throwable cause) {
        super(message, cause);
    }
}
