package tz.co.nbc.mypack.services;

import tz.co.nbc.mypack.cbsClientResponses.login.LoginOtp;

import java.util.ArrayList;
public interface OtpService {
    String generateToken();
}
