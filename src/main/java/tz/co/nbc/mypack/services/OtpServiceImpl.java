package tz.co.nbc.mypack.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tz.co.nbc.mypack.config.AppConfig;
import java.util.Random;

@Service
public class OtpServiceImpl implements OtpService {

    @Autowired
    private AppConfig appConfig;

    @Override
    public String generateToken() {
        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
}
