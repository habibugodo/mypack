package tz.co.nbc.mypack.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import tz.co.nbc.mypack.config.AppConfig;


import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import static tz.co.nbc.mypack.utils.DateUtils.convertToLocalDateTime;

@Component
public class JwtTokenProvider implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Autowired
    private AppConfig appConfig;


    public JwtResponse generateToken(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        Date expiry=jwtTokenExpiry();
        String token= Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS512, appConfig.getJWT_SECRET())
                .setIssuer("NbcMyPack")
                .compact();
        return new JwtResponse(token,convertToLocalDateTime(expiry));
    }

    public JwtResponse generateTokenByUser(Long userId) {

        Date expiry=jwtTokenExpiry();
        String token= Jwts.builder()
                .setSubject(Long.toString(userId))
                .setIssuedAt(new Date())
                .setExpiration(jwtTokenExpiry())
                .signWith(SignatureAlgorithm.HS512, appConfig.getJWT_SECRET())
                .setIssuer("NbcMyPack")
                .compact();
        return new JwtResponse(token,convertToLocalDateTime(expiry));
    }

    public String generateTokenByUserJwt(Long userId) {
        Date expiry=jwtTokenExpiry();
         return Jwts.builder()
                .setSubject(Long.toString(userId))
                .setIssuedAt(new Date())
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS512, appConfig.getJWT_SECRET())
                .setIssuer("NbcMyPack")
                .compact();

    }

    public Date jwtTokenExpiry() {
//        Date now = new Date();
//        Date expiryDate = new Date(now.getTime() + appConfig.getJWT_EXPIRATION());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE,appConfig.getJWT_EXPIRATION());
        return calendar.getTime();
    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(appConfig.getJWT_SECRET())
                .parseClaimsJws(token)
                .getBody();

        return Long.valueOf(claims.getSubject());
    }

    //Disable Jwt authentication
    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(appConfig.getJWT_SECRET()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
//            thrown
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
//            throw new BadRequestException("Toke Expired");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty");
        }
        return false;
    }



    public Jws<Claims> validateTokens(String authToken) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
          return  Jwts.parser().setSigningKey(appConfig.getJWT_SECRET()).parseClaimsJws(authToken);
    }

}
