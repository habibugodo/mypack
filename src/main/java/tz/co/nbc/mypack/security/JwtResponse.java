package tz.co.nbc.mypack.security;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class JwtResponse {
    private String token;
    private LocalDateTime tokenExpiration;
}