package tz.co.nbc.mypack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MypackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MypackApplication.class, args);
	}

}
