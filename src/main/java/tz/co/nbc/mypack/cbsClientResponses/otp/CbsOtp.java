package tz.co.nbc.mypack.cbsClientResponses.otp;


import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class CbsOtp {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {
        @SerializedName("nin")
        String nin;

        @SerializedName("current_phone_number")
        String currentPhoneNumber;
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        @SerializedName("responseCode")
        String responseCode;

        @SerializedName("message")
        String message;

        @SerializedName("TempCustomer")
        TempCustomer TempCustomer;

        @SerializedName("is_first_time")
        boolean isFirstTime=true;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TempCustomer {
        @SerializedName("reference_number")
        String referenceNumber;

        @SerializedName("nin")
        String nin;

        @SerializedName("current_phone_number")
        String currentPhoneNumber;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ApiResponse {
        String customerReference;
        String responseCode;
        boolean isFirstTime;
        String message;

    }
}
