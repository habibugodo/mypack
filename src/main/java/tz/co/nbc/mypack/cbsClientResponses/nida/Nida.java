package tz.co.nbc.mypack.cbsClientResponses.nida;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

public class Nida {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {
        @NotBlank
        private String fingerCode;
        @NotBlank
        private String fingerImage;
        @NotBlank
        private String customer;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {

        @SerializedName("responseCode")
        String responseCode;

        @SerializedName("message")
        String message;

        @SerializedName("customer")
        Customer customer;

        @SerializedName("token")
        String token;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Customer {
        @SerializedName("id")
        int id;

        @SerializedName("id_number")
        String idNumber;

        @SerializedName("id_type")
        String idType;

        @SerializedName("first_name")
        String firstName;

        @SerializedName("middle_name")
        String middleName;

        @SerializedName("last_name")
        String lastName;

        @SerializedName("gender")
        String gender;

        @SerializedName("resident_ward")
        String residentWard;

        @SerializedName("birth_region")
        String birthRegion;

        @SerializedName("birth_district")
        String birthDistrict;

        @SerializedName("birth_ward")
        String birthWard;

        @SerializedName("nationality")
        String nationality;

        @SerializedName("phone_number")
        String phoneNumber;

        @SerializedName("current_phone_number")
        String currentPhoneNumber;

        @SerializedName("current_email")
        String currentEmail;

        @SerializedName("marital_status")
        String maritalStatus;

        @SerializedName("title")
        String title;

        @SerializedName("occupation")
        String occupation;

        @SerializedName("job_position")
        String jobPosition;

        @SerializedName("employer_name")
        String employerName;

        @SerializedName("employer_address")
        String employerAddress;

        @SerializedName("monthly_income")
        String monthlyIncome;

        @SerializedName("education_level")
        String educationLevel;

        @SerializedName("spouse_name")
        String spouseName;

        @SerializedName("spouce_phone_number")
        String spoucePhoneNumber;

        @SerializedName("created_by")
        String createdBy;

        @SerializedName("created_at")
        Date createdAt;

        @SerializedName("status")
        String status;

        @SerializedName("otp")
        String otp;

        @SerializedName("is_otp_verified")
        boolean isOtpVerified;

        @SerializedName("otp_created_at")
        Date otpCreatedAt;

        @SerializedName("cif")
        String cif;

        @SerializedName("source_of_fund")
        String sourceOfFund;

        @SerializedName("working_industry")
        String workingIndustry;

        @SerializedName("preferred_language")
        String preferredLanguage;

        @SerializedName("ocp_references")
        String ocpReferences;

        @SerializedName("screening_response")
        String screeningResponse;

        @SerializedName("risk_rating_response")
        String riskRatingResponse;

        @SerializedName("onboarding_acknowledment_response")
        String onboardingAcknowledmentResponse;

        @SerializedName("onboarding_callback_response")
        String onboardingCallbackResponse;

        @SerializedName("duplicate_check_response")
        String duplicateCheckResponse;

        @SerializedName("photo")
        String photo;
    }
}
