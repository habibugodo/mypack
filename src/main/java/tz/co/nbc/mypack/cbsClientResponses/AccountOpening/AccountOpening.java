package tz.co.nbc.mypack.cbsClientResponses.AccountOpening;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class AccountOpening {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {

        @SerializedName("product_id")
        int productId;

        @SerializedName("branch_id")
        int branchId;

        @SerializedName("currency_code")
        String currencyCode;

        @SerializedName("need_mobile_banking")
        String needMobileBanking;

        @SerializedName("need_internet_banking")
        String needInternetBanking;

        @SerializedName("need_hellow_money")
        String needHellowMoney;

        @SerializedName("need_sms_alert")
        String needSmsAlert;

        @SerializedName("need_e_statement")
        String needEStatement;

        @SerializedName("need_debit_card")
        String needDebitCard;

        @SerializedName("description")
        String description;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        @SerializedName("description")
        String description;

    }
}
