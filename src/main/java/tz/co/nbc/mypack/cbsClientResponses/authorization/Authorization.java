package tz.co.nbc.mypack.cbsClientResponses.authorization;


import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;


public class Authorization {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {

        @SerializedName("emp_id")
        String empId;

        @SerializedName("password")
        String password;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        @SerializedName("responseCode")
        String responseCode;

        @SerializedName("message")
        String message;

        @SerializedName("initialData")
        InitialData initialData;

        @SerializedName("is_first_time_pin")
        boolean isFirstTimePin;

        @SerializedName("user")
        User user;

        @SerializedName("token")
        String token;

        @SerializedName("expiry")
        long expiry;

    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InitialData {
        @SerializedName("products")
        private List<Product> products = new ArrayList<Product>();

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Product {
        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("category")
        String category;

        @SerializedName("code")
        String code;

        @SerializedName("currency_code")
        String currencyCode;

        @SerializedName("description")
        String description;

        @SerializedName("features")
        String features;

        @SerializedName("constraints")
        String constraints;

        @SerializedName("target_age_group")
        String targetAgeGroup;

        @SerializedName("target_employment_group")
        String targetEmploymentGroup;

        @SerializedName("photo")
        String photo;

        @SerializedName("name_sw")
        String nameSw;

        @SerializedName("description_sw")
        String descriptionSw;

        @SerializedName("feature_sw")
        String featureSw;

        @SerializedName("benefits_sw")
        String benefitsSw;

        @SerializedName("benefits")
        String benefits;

        @SerializedName("constraints_sw")
        String constraintsSw;

        @SerializedName("photo_sw")
        String photoSw;

        @SerializedName("readmore_link_sw")
        String readmoreLinkSw;

        @SerializedName("readmore_link")
        String readmoreLink;

    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class User {
        @SerializedName("name")
        String name;

        @SerializedName("emp_id")
        String empId;

        @SerializedName("email")
        String email;

        @SerializedName("branch_code")
        String branchCode;

        @SerializedName("mobile")
        int mobile;

        @SerializedName("role")
        String role;

    }
}