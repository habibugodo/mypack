package tz.co.nbc.mypack.cbsClientResponses.branch;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

public class Branch {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("code")
        String code;

        @SerializedName("districts")
        List<Districts> districts;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Districts {

        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("description")
        String description;

        @SerializedName("region_id")
        Long regionId;

        @SerializedName("wards")
        List<Wards> wards;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Wards {

        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("offices")
        List<Offices> offices;
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Offices {

        @SerializedName("id")
        int id;

        @SerializedName("type")
        String type;

        @SerializedName("title")
        String title;

        @SerializedName("ward_id")
        Long wardId;

        @SerializedName("longitude")
        String longitude;

        @SerializedName("latitude")
        String latitude;

        @SerializedName("telephone")
        String telephone;

        @SerializedName("address")
        String address;

        @SerializedName("created_by")
        Long createdBy;

        @SerializedName("created_at")
        Date createdAt;

        @SerializedName("status")
        String status;

        @SerializedName("code")
        String code;
    }
}
