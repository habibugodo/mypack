package tz.co.nbc.mypack.cbsClientResponses.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class LoginOtp {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {
        String mobile;
        String name;
        String preferred_language;
        String otp;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        String responseCode;
        String message;
        String data;
    }

}
