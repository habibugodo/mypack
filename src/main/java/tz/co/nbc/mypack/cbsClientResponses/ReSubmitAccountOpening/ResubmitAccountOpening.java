package tz.co.nbc.mypack.cbsClientResponses.ReSubmitAccountOpening;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ResubmitAccountOpening {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {
        @SerializedName("application_id")
        int applicationId;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        @SerializedName("description")
        String description;
    }
}
