package tz.co.nbc.mypack.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;

public class JwtReader {

    static Logger logger = LogManager.getLogger(JwtReader.class);


    public static  Date readToken(String tokens){
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
        Claims decodedToken = Jwts.parser().setSigningKey("secret").parseClaimsJws(token).getBody();
        long expirationTime = decodedToken.getExpiration().getTime();
        return new Date(expirationTime * 1000);
    }

    public static Date getExpirationTime(String token) {
        String[] parts = token.split("\\.");
        String payload = new String(Base64.getDecoder().decode(parts[1]));
        String[] payloadParts = payload.split(",");
        String[] expParts = payloadParts[1].split(":");
        long expTime = Long.parseLong(expParts[1].replaceAll("[^\\d]", ""));
        return new Date(expTime * 1000);
    }

    public static Date getExpirationTimeNew(String token) {
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String[] chunks = token.split("\\.");

        String header = new String(decoder.decode(chunks[0]));
        String payload = new String(decoder.decode(chunks[1]));

        String[] payloadParts = payload.split(",");
        String[] expParts = payloadParts[1].split(":");
        logger.info("Jwt Parts: "+payloadParts.toString());
        logger.info("Parts"+payloadParts[22]); //18
        String[] keyValue=payloadParts[22].split(":");
        long time=Long.parseLong(keyValue[1].replaceAll("[^\\d]", ""));
        return new Date(time * 1000);
    }

    public static Date getDateFromString(long expiry){
        return new Date(expiry * 1000);
    }
    public static LocalDateTime convertDateToLocalDateTime(Date dateToConvert) {
        ZoneId zone = ZoneId.of("Africa/Dar_es_Salaam");
        return dateToConvert.toInstant()
                .atZone(zone)
                .toLocalDateTime();
    }
}
