package tz.co.nbc.mypack.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

@Component
public class ApiHttpClient {

    static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    Logger logger = LogManager.getLogger(ApiHttpClient.class);

    HttpClient httpClient = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(60))
            .executor(Executors.newFixedThreadPool(3))
            .sslContext(getSslContext())
            .build();

//    {
//        "emp_id":"NBCMYPACK",
//            "password":"NbcMyPack@2023!"
//    }
    @Async
    public CompletableFuture<HttpResponse<String>> GetAuthorizationToken(String API_URL,String body) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

//    {
//        "nin": "19110509111010000115",
//            "current_phone_number": 766192332
//    }
    @Async
    public CompletableFuture<HttpResponse<String>> RegisterNewCustomer(String API_URL,String body,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

//    {
//        "current_phone_number": "766192332",
//            "otp": "6222"
//    }

    @Async
    public CompletableFuture<HttpResponse<String>> verifyMobileNumber(String API_URL,String body,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

    //Provides a customer token to be used in the future;
    //Using customer token
    @Async
    public CompletableFuture<HttpResponse<String>> UpdateCustomerInformation(String API_URL,String body,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

    @Async
    public CompletableFuture<HttpResponse<String>> UpdateCustomerInformationAndApplication(String API_URL,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString("{}"))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

    @Async
    public CompletableFuture<HttpResponse<String>> NidaKycQuery(String API_URL,String body,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

    @Async
    public CompletableFuture<HttpResponse<String>> getCustomerProducts(String API_URL,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString("{}"))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }


    @Async
    public CompletableFuture<HttpResponse<String>> customerApplicationApply(String API_URL,String body,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }

    @Async
    public CompletableFuture<HttpResponse<String>> customerApplicationResubmit(String API_URL,String body,String token) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyStoreException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Bearer ",token)
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        logger.info("Response: "+response.body());
        return CompletableFuture.completedFuture(response);
    }



    private SSLContext getSslContext(){
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            return sslContext;
        }catch (Exception e){
            System.out.println("Ssl Exception: "+e);
        }
        return null;
    }

    private static TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };


//    private SSLConnectionSocketFactory getSslContext2() {
//        try {
//            SSLContextBuilder builder = new SSLContextBuilder();
//            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
//            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
//                    builder.build());
//            return sslsf.;
//        }catch (Exception e){
//            System.out.println("Ssl Exception"+e);
//        }
//        return null;
//    }
}

