package tz.co.nbc.mypack.utils;


public class Constants {

    public static final String API_V1 = "/api/v1";
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public  static final String JSON_HEADER = "application/json";
}
