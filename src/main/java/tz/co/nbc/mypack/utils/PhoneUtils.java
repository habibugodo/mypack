package tz.co.nbc.mypack.utils;

public class PhoneUtils {

    public static String getMobileNumberWithCodeNoPlus(String number) {
        String numb = number != null ? number.trim() : number;
        if (numb != null) {
            int len = numb.length();
            if (len >= 9)
                numb = "255" + numb.substring(numb.length() - 9);
        }
        return numb;
    }

    public static String getMobileNumberNbcFormat(String number) {
        String numb = number != null ? number.trim() : number;
        if (numb != null) {
            int len = numb.length();
            if (len >= 9)
                numb =numb.substring(numb.length() - 9);
        }
        return numb;
    }
}
