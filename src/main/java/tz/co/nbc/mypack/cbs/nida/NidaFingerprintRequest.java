package tz.co.nbc.mypack.cbs.nida;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class NidaFingerprintRequest {
    private String finger_type;
    private String fingerprint_image;
}
