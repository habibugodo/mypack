package tz.co.nbc.mypack.cbs.nida;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NidaFingerprintResponse {
    private String responseCode;
    private String message;
    private Customer customer;
    private String token;
}


@Data
@AllArgsConstructor
@NoArgsConstructor
 class Customer {
    private Integer id;
    private String idNumber;
    private String idType;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String residentWard;
    private String birthRegion;
    private String birthDistrict;
    private String birthWard;
    private String nationality;
    private String phoneNumber;
    private String currentPhoneNumber;
    private String currentEmail;
    private String maritalStatus;
    private String title;
    private String occupation;
    private String jobPosition;
    private String employerName;
    private String employerAddress;
    private String monthlyIncome;
    private String educationLevel;
    private String spouseName;
    private String spoucePhoneNumber;
    private String createdBy;
    private String createdAt;
    private String status;
    private String cif;
    private String sourceOfFund;
    private String workingIndustry;
    private String ocpReferences;
    private String screeningResponse;
    private String riskRatingResponse;
    private String onboardingAcknowledmentResponse;
    private String onboardingCallbackResponse;
    private String photo;
}
