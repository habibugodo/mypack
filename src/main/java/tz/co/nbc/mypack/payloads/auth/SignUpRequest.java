package tz.co.nbc.mypack.payloads.auth;

import lombok.Data;
import lombok.NoArgsConstructor;
import tz.co.nbc.mypack.validators.PasswordValueMatch;
import tz.co.nbc.mypack.validators.Phone;
import tz.co.nbc.mypack.validators.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@PasswordValueMatch.List({
        @PasswordValueMatch(
                field = "password",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
@Data
@NoArgsConstructor
public class SignUpRequest {

    //Check full Name
    @NotBlank
    private String fullName;

//    @NotBlank
//    @Size(min=6,message="Username length should be at least 6 characters")
//    private String username;

    @NotBlank
    @Phone
    private String phoneNumber;

    @NotBlank
    @Email(message="Valid Email is required")
    private String email;

    @NotBlank
    @ValidPassword
    @Size(min=6,message="Password length should be at least 6 characters")
    private String password;

    @NotBlank
    @ValidPassword
    @Size(min=6,message="Password length should be at least 6 characters")
    private String confirmPassword;
}