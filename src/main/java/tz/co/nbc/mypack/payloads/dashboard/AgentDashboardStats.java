package tz.co.nbc.mypack.payloads.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentDashboardStats {

    private Long totalOpenedAccounts;
    private Long totalPEPAccounts;
    private Long successfullyOpenedAccounts;
    private Long totalSalesDiaries;
    private Long totalNextAppointments;
}