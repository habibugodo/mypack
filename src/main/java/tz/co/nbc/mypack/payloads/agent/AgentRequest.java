package tz.co.nbc.mypack.payloads.agent;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import tz.co.nbc.mypack.validators.Phone;

@Data
@NoArgsConstructor
public class AgentRequest {

    //Agent image Url
//    private String profileUrl;
//    private String profileImageType;

    @NotBlank
    private String fullName;

    @NotBlank
    @Phone
    private String phoneNumber;

    @NotNull
    private Long branchId;

    @NotBlank
    @Email(message="Valid Email is required")
    private String email;

    @NotBlank
    @Size(min=6,message="Password length should be at least 6 characters")
    private String password;

    @NotBlank
    @Size(min=6,message="Password length should be at least 6 characters")
    private String confirmPassword;
}