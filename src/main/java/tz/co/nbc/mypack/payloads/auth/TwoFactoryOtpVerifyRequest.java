package tz.co.nbc.mypack.payloads.auth;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
public class TwoFactoryOtpVerifyRequest {

    @NotNull
    private Long userId;

    @NotBlank
    private String phoneNumber;

    @NotBlank
    private String otp;
}

