package tz.co.nbc.mypack.payloads.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

public class LoginResponse {
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Response {
        private UserResponse user;
        private String tokenType="Bearer";
        private String token;
        private String tokenExpiration;
        private String refreshToken;
    }


    @Data
    @NoArgsConstructor
    public static class UserResponse {
        private Long id;
        private String fullName;
        private String email;
        private String phoneNumber;
        private String role;
        private Long departmentId;
        private String department;
        private Boolean passwordChanged;
        private LocalDateTime registrationDate;
        private String status;
    }

}