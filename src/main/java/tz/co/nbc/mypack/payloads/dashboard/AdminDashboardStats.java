package tz.co.nbc.mypack.payloads.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminDashboardStats {

    private Long totalAgents;
    private Long totalSalesDiaries;
    private Long totalCustomers;
    private Long totalBranches;
    private Long totalCardTypes;
    private Long totalProducts;

}