package tz.co.nbc.mypack.payloads.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentApprovalRequest {

    @NotNull
    private Long agentId;

    @NotBlank
    private String remarks;
}