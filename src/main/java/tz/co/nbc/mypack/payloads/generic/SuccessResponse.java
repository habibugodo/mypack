package tz.co.nbc.mypack.payloads.generic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SuccessResponse {
    private String success;
    private String successDescription;
}
