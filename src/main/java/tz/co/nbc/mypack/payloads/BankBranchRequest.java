package tz.co.nbc.mypack.payloads;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankBranchRequest {
    @NotBlank
    private String name;
    @NotBlank
    private String branchCode;
    @NotBlank
    private String branchId;
}