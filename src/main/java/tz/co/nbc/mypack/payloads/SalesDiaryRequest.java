package tz.co.nbc.mypack.payloads;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesDiaryRequest {

    private Long id;

    @NotNull
    private Long topicId;

    @NotBlank
    private String description;

    @NotBlank
    private String customerName;

    @NotBlank
    private String customerType; //Individual,Company,Organization

    @NotBlank
    private String phoneNumber;

    @NotNull
    private int numberOfPotentialCustomer;

    @NotNull
    private String email;

    @NotNull
    private int rating;

    @NotNull
    private String location;

    @NotBlank
    private String address;

    @NotNull
//    @FutureOrPresent(message = "Date should be in the future")
//    @JsonFormat(pattern = "dd-MM-yyyy")
    private String nextAppointment;

    @NotNull
    private Boolean hasNextAppointment;
}