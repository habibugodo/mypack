package tz.co.nbc.mypack.payloads.generic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordPage {
    private String pageNo;
    private String pageSize;
    private String totalSize;
    private String pageCount;
}