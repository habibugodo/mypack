package tz.co.nbc.mypack.payloads.generic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse<T> {
    private RecordPage page;
    private List<T> records;
}
