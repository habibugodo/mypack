package tz.co.nbc.mypack.payloads.generic;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;


public final class MagicPagination<T> {

    private final Page<T> t;

    private MagicPagination(Page<T> t) {
        this.t = t;
    }

    public static <T> MagicPagination of(Page<T> t) {
        return new MagicPagination(t);
    }

    public ResponseEntity<?> paginate() {

        PageResponse<T> pageResponse = new PageResponse<>();
        pageResponse.setPage(new RecordPage(
                String.valueOf(t.getNumber()),
                String.valueOf(t.getNumberOfElements()),
                String.valueOf(t.getTotalElements()),
                String.valueOf(t.getTotalPages() - 1)));
        if (t.isEmpty()) {
            pageResponse.setRecords(new ArrayList<>());
        } else {
            pageResponse.setRecords(t.get().toList());
        }
        return new ResponseEntity<>(pageResponse, HttpStatus.OK);

    }
}