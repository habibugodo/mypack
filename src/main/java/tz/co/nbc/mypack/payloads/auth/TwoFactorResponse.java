package tz.co.nbc.mypack.payloads.auth;

import lombok.Data;

@Data
public class TwoFactorResponse {
    private String phoneNumber;
    private Long userId;
    private String message;
}