package tz.co.nbc.mypack.payloads.auth;

import lombok.Data;
import lombok.NoArgsConstructor;
import tz.co.nbc.mypack.validators.PasswordValueMatch;
import tz.co.nbc.mypack.validators.ValidPassword;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@PasswordValueMatch.List({
        @PasswordValueMatch(
                field = "password",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
@Data
@NoArgsConstructor
public class AdminPasswordResetRequest {

    private Long userId;
    @NotBlank
    @ValidPassword
    @Size(min=6,message="Password length should be at least 6 characters")
    private String password;


    @NotBlank
    @ValidPassword
    @Size(min=6,message="Password length should be at least 6 characters")
    private String confirmPassword;
}