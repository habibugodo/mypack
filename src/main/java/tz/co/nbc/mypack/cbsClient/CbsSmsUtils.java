package tz.co.nbc.mypack.cbsClient;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tz.co.nbc.mypack.cbsClientResponses.login.LoginOtp;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.CbsTokenType;
import tz.co.nbc.mypack.models.AppStorage;
import tz.co.nbc.mypack.models.SmsLog;
import tz.co.nbc.mypack.repositories.AppStorageRepository;
import tz.co.nbc.mypack.repositories.SmsLogsRepository;

import java.util.Optional;

import static tz.co.nbc.mypack.config.AppConfig.CBS_API_URL;

@Component
public class CbsSmsUtils {

    Logger logger = LogManager.getLogger(CbsSmsUtils.class);

    @Autowired
    private CbsClient cbsClient;

    @Autowired
    private AppStorageRepository appStorageRepository;

    @Autowired
    private SmsLogsRepository smsLogsRepository;

    static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public boolean sendSms(LoginOtp.Request request){

        //String CBS_API_URL = "https://nacbe-v001-nbc-alternative-channels.apps.ocp4-kawe.tz.af.absa.local/api/v1";
//        String CBS_API_URL="https://extgtw.nbc.co.tz/nacbe-mypack/1.0.0";
        String url = CBS_API_URL + "/Notifications/Vendors/OTP";
        String content = gson.toJson(request);
        logger.info("Otp: "+content);



        Optional<AppStorage> appStorage = this.appStorageRepository.findByItemKey(CbsTokenType.CBS_TOKEN.name());
        if(appStorage.isEmpty()){
            logger.info("CBS Token is missing");
        }

        if(appStorage.isPresent()) {
            CbsClientRequest cbsClientRequest = new CbsClientRequest();
            cbsClientRequest.setContent(content);
            cbsClientRequest.setUrl(url);
            cbsClientRequest.setToken(appStorage.get().getItemDescription());
            cbsClientRequest.setRequireToken(true);
            cbsClientRequest.setTokenType(CbsTokenType.CBS_TOKEN.name());


            CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
            if (cbsClientResponse.isSuccess()) {
                logger.info("Success: " + cbsClientResponse.getContent());
                SmsLog smsLog=new SmsLog();
                smsLog.setSmsType("OTP");
                smsLog.setMessage(request.getOtp());
                smsLog.setPhoneNumber(request.getMobile());
                smsLog.setSender("NBC");
                this.smsLogsRepository.save(smsLog);
                logger.info("Sent sms successfully: "+content);
                return true;
                //Save Sms
            } else {
                logger.info("Error: " + cbsClientResponse.getError());
                logger.error("Sent sms failed ");
                return false;
            }
        }

        return false;
    }
}
