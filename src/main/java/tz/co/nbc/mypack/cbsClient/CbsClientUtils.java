package tz.co.nbc.mypack.cbsClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tz.co.nbc.mypack.cbsClientResponses.authorization.Authorization;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.CbsTokenType;
import tz.co.nbc.mypack.models.AppStorage;
import tz.co.nbc.mypack.models.optionsData.BankProduct;
import tz.co.nbc.mypack.repositories.AppStorageRepository;
import tz.co.nbc.mypack.repositories.optionsData.BankProductsRepository;
import tz.co.nbc.mypack.utils.GsonHelper;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static tz.co.nbc.mypack.utils.JwtReader.*;


@Component
public class CbsClientUtils {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private CbsClient cbsClient;

    @Autowired
    private AppStorageRepository appStorageRepository;

    @Autowired
    private BankProductsRepository bankProductsRepository;


//    static Gson gson = new GsonBuilder().registerTypeAdapterFactory(new GsonHelper.NullStringToEmptyAdapterFactory()).create();

    static Gson gson = new GsonBuilder().registerTypeAdapterFactory(new GsonHelper.NullStringToEmptyAdapterFactory()).setPrettyPrinting().disableHtmlEscaping().create();
//    static Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();

    private static final Logger logger = LoggerFactory.getLogger(CbsClient.class);
//    private String CBS_API_URL="https://nacbe-v001-nbc-alternative-channels.apps.ocp4-kawe.tz.af.absa.local/api/v1";
    private String CBS_API_URL="https://internal-gateway.apps.ocp4.tz.af.absa.local:443/nacbev001-vendorsacopening/1.0.0";

    public void Authenticate() {
        String url = CBS_API_URL+ "/Authentication/Portal/Vendors/Authenticate";
        Authorization.Request authorizationRequest = new Authorization.Request("NBCMYPACK", "NbcMyPack@2023!");
        String content = gson.toJson(authorizationRequest);

        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken("");
        cbsClientRequest.setTokenType(CbsTokenType.CBS_TOKEN.name());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            logger.info("Cbs response: "+cbsClientResponse.getContent());
            Authorization.Response authorizationResponse = gson.fromJson(cbsClientResponse.getContent(), Authorization.Response.class);
            AppStorage appStorage=this.appStorageRepository.findByItemKey(CbsTokenType.CBS_TOKEN.name()).orElse(new AppStorage());

            String token=authorizationResponse.getToken();
//            Date date=getExpirationTimeNew(token);
            Date date=getDateFromString(authorizationResponse.getExpiry());
            LocalDateTime localDateTime=convertDateToLocalDateTime(date);
            appStorage.setItemKey(CbsTokenType.CBS_TOKEN.name());
            appStorage.setItemDescription(authorizationResponse.getToken());
            appStorage.setExpiration(localDateTime);
            this.appStorageRepository.save(appStorage);
            this.bankProductsRepository.deleteAll();
            bankProduct(authorizationResponse.getInitialData().getProducts());
        } else {
            logger.info("Could not authenticate to the core banking system");
        }

    }

    private void bankProduct(List<Authorization.Product> products) {
        for (Authorization.Product product : products) {
            BankProduct bankProduct = new BankProduct();
            bankProduct.setProductId((long) product.getId());
            bankProduct.setName(product.getName());
            bankProduct.setCategory(product.getCategory());
            bankProduct.setCode(product.getCode());
            bankProduct.setCurrencyCode(product.getCurrencyCode());
            bankProduct.setDescription(product.getDescription());
            bankProduct.setFeatures(product.getFeatures());
            bankProduct.setConstraints(product.getConstraints());
            bankProduct.setTargetAgeGroup(product.getTargetAgeGroup());
            bankProduct.setTargetEmploymentGroup(product.getTargetAgeGroup());
            bankProduct.setPhoto(product.getPhoto());
            bankProduct.setNameSw(product.getNameSw());
            bankProduct.setDescriptionSw(product.getDescriptionSw());
            bankProduct.setBenefitsSw(product.getBenefitsSw());
            bankProduct.setBenefits(product.getBenefits());
            bankProduct.setConstraintsSw(product.getConstraintsSw());
            bankProduct.setPhotoSw(product.getPhotoSw());
            bankProduct.setReadmoreLink(product.getReadmoreLink());
            bankProduct.setReadmoreLinkSw(product.getReadmoreLinkSw());
            this.bankProductsRepository.save(bankProduct);
        }
    }
}
