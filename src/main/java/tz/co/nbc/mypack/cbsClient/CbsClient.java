package tz.co.nbc.mypack.cbsClient;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.CbsTokenType;
import tz.co.nbc.mypack.exceptions.CoreBankingException;
import tz.co.nbc.mypack.models.AppStorage;
import tz.co.nbc.mypack.payloads.generic.ErrorResponse;
import tz.co.nbc.mypack.repositories.AppStorageRepository;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.Executors;
@Component
public class CbsClient {


    private static final Logger logger = LoggerFactory.getLogger(CbsClient.class);

    @Autowired
    private CbsClientUtils cbsClientUtils;

    @Autowired
    private AppStorageRepository appStorageRepository;

    @Autowired
    private AppConfig appConfig;

    HttpClient httpClient = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(60))
            .executor(Executors.newFixedThreadPool(10))
            .sslContext(getSslContext())
            .build();


    public CbsClientResponse sendHttp(CbsClientRequest cbsClientRequest){
        try {
            if(!cbsClientRequest.getUrl().contains("/Authentication/Portal/Vendors/Authenticate")) {
                if (cbsClientRequest.getTokenType().equalsIgnoreCase(CbsTokenType.CBS_TOKEN.name())) {
                    if (cbsClientRequest.getToken().isBlank() || (cbsClientRequest.getTokenExpiration().isAfter(LocalDateTime.now()))) {
                        cbsClientUtils.Authenticate();
                    }
                }
                Optional<AppStorage> appStorage = this.appStorageRepository.findByItemKey(CbsTokenType.CBS_TOKEN.name());
                if (appStorage.isEmpty()) {
                   logger.info("Bearer token is missing");
                }else {
                    logger.info("Bearer Token: "+appStorage.get().getItemDescription());
                    cbsClientRequest.setToken(appStorage.get().getItemDescription());
                    cbsClientRequest.setTokenExpiration(appStorage.get().getExpiration());
                }
            }

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(cbsClientRequest.getUrl()))
                    .header("Content-Type", "application/json")
                    .header("Authorization","Bearer "+cbsClientRequest.getToken())
//                    .headers(headers)
                    .POST(HttpRequest.BodyPublishers.ofString(cbsClientRequest.getContent(), StandardCharsets.UTF_8))
                    .build();
            logger.info("Request: "+request);
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            logger.info("Response: "+response);
            CbsClientResponse cbsClientResponse=new CbsClientResponse();
            if (response.statusCode() == 200) {
                logger.info("Response Entity : "+response.body());
                cbsClientResponse.setContent(response.body());
                cbsClientResponse.setSuccess(true);
            }else {
                cbsClientResponse.setContent(response.body());
                cbsClientResponse.setSuccess(false);
            }
            return cbsClientResponse;
        }
        catch (IOException e){
            logger.info("Core banking system is down");
            throw new CoreBankingException("Exception: Unable to reach the core banking system");
        }
        catch (Exception e){
            logger.info("Exception on httpclient : "+e.getMessage());
            throw new CoreBankingException("Unable to reach the core banking system");
        }

    }






    private SSLContext getSslContext(){
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            return sslContext;
        }catch (Exception e){
            System.out.println("Ssl Exception: "+e);
        }
        return null;
    }

    private static TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };
}
