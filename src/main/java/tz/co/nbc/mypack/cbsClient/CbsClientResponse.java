package tz.co.nbc.mypack.cbsClient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsClientResponse {
    private String content;
    private String error;
    private boolean isSuccess;
}
