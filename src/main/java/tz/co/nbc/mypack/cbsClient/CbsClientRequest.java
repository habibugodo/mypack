package tz.co.nbc.mypack.cbsClient;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsClientRequest {
    private String url;
    private String content;
    private String token;
    private String tokenType; //CustomerToken ,CbsToken
    private LocalDateTime tokenExpiration=LocalDateTime.now().minus(3, ChronoUnit.HOURS);
    private boolean requireToken=true;
}
