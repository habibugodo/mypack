package tz.co.nbc.mypack.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemReqeust {

    private Long id;
    @NotBlank
    private String name;

}