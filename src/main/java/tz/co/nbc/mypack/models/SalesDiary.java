package tz.co.nbc.mypack.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name="SALES_DIARIES")
public class SalesDiary {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    //Category
    //Loan ,Customer Acqusitions
    @Column(name = "DIARY_TOPIC")
    private String topic;

    @Column(name = "DIARY_TOPIC_ID")
    private Long topicId;

    @Column(name="DESCRIPTION",columnDefinition="TEXT")
    private String description;

    @Column(name = "CUSTOMER_NAME")
    private String customerName;

    @Column(name = "CUSTOMER_TYPE")
    private String customerType; //Individual,Company,Organization

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "NUMBER_OF_POTENTIAL_CUSTOMER")
    private Integer numberOfPotentialCustomer;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "RATING")
    private int rating;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "ADDRESS")
    private String address;

    //Send email a day before the next appointment
    //A message to remember the next appointment
    @Column(name = "NEXT_APPOINTMENT")
    private String nextAppointment;

    @Column(name = "HAS_NEXT_APPOINTMENT")
    private Boolean hasNextAppointment;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "AGENT_ID")
    private Long agentId;

    @Column(name = "AGENT_NAME")
    private String agentName;

    @Column(name = "BRANCH")
    private Long branch;

    @Column(name = "BRANCH_NAME")
    private String branchName;

    @CreationTimestamp
    @Column(name = "CREATED_AT")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "UPDATED_AT")
    private Timestamp updatedAt;
}