package tz.co.nbc.mypack.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import tz.co.nbc.mypack.enums.TokenType;


import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "TOKENS",indexes = @Index(columnList = "ID,USER_ID,TOKEN",name = "INDEX_TOKEN"))
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Token {


    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "TOKEN_TYPE")
    @Enumerated(EnumType.STRING)
    private TokenType tokenType;

    @Column(name = "TOKEN_EXPIRATION")
    private LocalDateTime tokenExpiration;

    @Column(name = "REVOKED")
    private boolean revoked;

    @Column(name = "EXPIRED")
    private boolean expired;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "USERNAME")
    private String username;

    @CreationTimestamp
    @Column(name = "UPDATED_AT")
    private LocalDateTime updatedAt;

    @CreationTimestamp
    @Column(name = "CREATED_AT")
    private LocalDateTime createdAt;

}
