package tz.co.nbc.mypack.models.optionsData;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Data
@Entity
@Table(name="BANK_BRANCHES")
public class BankBranch {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "BRANCH_CODE")
    private String branchCode;

    @Column(name = "BRANCH_ID")
    private String branchId;

    @Column(name = "REGION")
    private String region;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "WARD")
    private String ward;
}