package tz.co.nbc.mypack.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;


@Data
@Entity
@Table(name="TEMPORARY_CUSTOMER")
public class TemporaryCustomer {

    //isPep
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "CUSTOMER_ID")
    private String customerId;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "NIN_NUMBER")
    private String ninNumber;

    @JsonIgnore
    @Column(name = "TOKEN",columnDefinition="TEXT")
    private String token;

    @Column(name = "TOKEN_TYPE")
    private String tokenType;

    @Column(name = "IS_FIRST_TIME")
    private boolean isFirstTime;

    @Column(name = "APPLICATION_STAGE")
    private String applicationStage;

    @Column(name = "AGENT")
    private Long agent;

    @Column(name = "AGENT_NAME")
    private String agentName;
}