package tz.co.nbc.mypack.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name="APP_STORAGE")
@NoArgsConstructor
public class AppStorage {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "ITEM_KEY")
    private String itemKey;

    @Column(name = "ITEM_DESCRIPTION",columnDefinition="TEXT")
    private String itemDescription;

    @Column(name = "EXPIRATION")
    private LocalDateTime expiration=LocalDateTime.now();

    public AppStorage(String itemKey, String itemDescription, LocalDateTime expiration) {
        this.itemKey = itemKey;
        this.itemDescription = itemDescription;
        this.expiration = expiration;
    }

    //    token type CUSTOMER_TOKEN,CBS_TOKEN

}
