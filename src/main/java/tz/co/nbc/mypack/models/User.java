package tz.co.nbc.mypack.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "USERS",indexes = @Index(columnList = "ID,EMAIL,PHONE_NUMBER",name = "INDEX_USERS"),
        uniqueConstraints = { @UniqueConstraint(name = "UniquePhoneNumberAndEmail", columnNames = { "PHONE_NUMBER", "EMAIL" }) })
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "FULL_NAME")
    private String fullName="";

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @JsonIgnore
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "IS_ACTIVE")
    private boolean isActive;

    @Column(name = "IS_DELETED")
    private boolean isDeleted;

    @JsonIgnore
    @Column(name = "ACCOUNT_LOCK_OUT")
    private Integer accountLockOut;

    @JsonIgnore
    @Column(name = "RESET_LOCK_OUT")
    private Integer resetLockOut;

    @JsonIgnore
    @Column(name = "INVALID_LOGINS_TO_LOCK")
    private Integer invalidLoginsToLock=0;

    @Column(name = "REQUIRE_PASSWORD_CHANGE")
    private Boolean requirePasswordChange=true;

    @Column(name = "PASSWORD_CHANGED")
    private Boolean passwordChanged=false;


    @CreatedDate
    @Column(name = "REGISTRATION_DATE")
    private LocalDateTime registrationDate;

    @UpdateTimestamp
    @Column(name = "UPDATED_AT")
    private LocalDateTime updatedAt;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "MULTI_DEVICE_LOGIN")
    private boolean multiDeviceLogin=true;

    @JsonIgnore
    @Column(name = "REFRESH_TOKEN")
    private String refreshToken;

    @JsonIgnore
    @Column(name = "REFRESH_TOKEN_EXPIRATION")
    private LocalDateTime refreshTokenExpiration=LocalDateTime.now();

    @JsonIgnore
    @Column(name = "PASSWORD_RESET_TOKEN")
    private String passwordResetToken;

    @JsonIgnore
    @Column(name = "PASSWORD_RESET_TOKEN_EXPIRATION")
    private LocalDateTime passwordResetTokenExpiration;

    @JsonIgnore
    @Column(name = "OTP_TOKEN")
    private String otpToken;

    @JsonIgnore
    @Column(name = "OTP_TOKEN_EXPIRATION")
    private LocalDateTime otpTokenExpiration;

    @Column(name = "ROLE")
    private String role; //CompanyAdmin


    @Column(name = "BRANCH")
    private Long branch;

    @Column(name = "BRANCH_NAME")
    private String branchName;

    //Use jms send last login update
    @Column(name = "LAST_LOGIN")
    private String lastLogin;

    //Agents approval
    @Column(name = "APPROVED_BY")
    private Long approvedBy=0L;

    @Column(name = "APPROVED_BY_NAME")
    private String approveByName="";

    @Column(name = "APPROVAL_STATUS_NAME")
    private String approvalStatus;

    @Column(name = "APPROVED_AT")
    private Instant approvedAt;

    @Column(name = "APPROVAL_REMARKS",columnDefinition="TEXT")
    private String approvedRemarks;

}