package tz.co.nbc.mypack.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.sql.Timestamp;


@Data
@Entity
@Table(name="CUSTOMER")
public class Customer {

    //isPep
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "NIN_NUMBER")
    private String ninNumber;

    @Column(name = "ACCOUNT_TYPE")
    private String accountType;

    @Column(name = "CARD_TYPE")
    private String cardType;

    @Column(name = "ACCOUNT_NUMBER")
    private String AccountNumber;

    @Column(name = "CITIZENSHIP")
    private String citizenship;

    @Column(name = "LEADER_SHIP_STATUS")
    private String leaderShipStatus;

    @Column(name = "EMPLOYMENT_STATUS")
    private String employmentStatus;

    @Column(name = "MARITAL_STATUS")
    private String maritalStatus;

    @Column(name = "INSTITUTIONAL_NAME")
    private String institutionalName;

    @Column(name = "SALARY")
    private int salary;

    @Column(name = "BRANCH_ID")
    private Long branchId;

    @Column(name = "BRANCH_NAME")
    private String branchName;

    @Column(name = "SIGNATURE_IMAGE_URL")
    private String signatureImageURL;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "IS_PEP")
    private boolean isPEP;

    @Column(name = "AGENT_ID")
    private Long agentId;

    @Column(name = "AGENT_NAME")
    private String agentName;

    @CreationTimestamp
    @Column(name = "CREATED_AT")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "UPDATED_AT")
    private Timestamp updatedAt;
}