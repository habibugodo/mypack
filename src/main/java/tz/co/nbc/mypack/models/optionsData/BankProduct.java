package tz.co.nbc.mypack.models.optionsData;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Data
@Entity
@Table(name="BANK_PRODUCTS")
public class BankProduct {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CATEGORY")
    private String  category;

    @Column(name = "CODE")
    private String  code;

    @Column(name = "CURRENCY_CODE")
    private String currencyCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "FEATURES")
    private String features;

    @Column(name = "CONSTRAINTS")
    private String constraints;

    @Column(name = "TARGET_AGE_GROUP")
    private String targetAgeGroup;

    @Column(name = "TARGET_EMPLOYMENT_GROUP")
    private String targetEmploymentGroup;

    @Column(name = "PHOTO")
    private String photo;

    @Column(name = "NAME_SW")
    private String nameSw;

    @Column(name = "DESCRIPTION_SW")
    private String descriptionSw;

    @Column(name = "FEATURE_SW")
    private String featureSw;

    @Column(name = "BENEFIT_SW",columnDefinition="TEXT")
    private String benefitsSw;

    @Column(name = "BENEFIT",columnDefinition="TEXT")
    private String benefits;

    @Column(name = "CONSTRAINTS_SW")
    private String constraintsSw;

    @Column(name = "PHOTO_SW")
    private String photoSw;

    @Column(name = "READ_MORE_LINK")
    private String readmoreLink;

    @Column(name = "READ_MORE_LINK_SW")
    private String readmoreLinkSw;


}