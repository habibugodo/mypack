package tz.co.nbc.mypack.models;


public enum AccountStatus {
    PENDING(0),ACTIVE(1), DISABLED(2),REJECTED(3);
    private final int status;
    AccountStatus(int status) {
        this.status=status;
    }

    public int getStatus() {
        return status;
    }
}