package tz.co.nbc.mypack.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.sql.Timestamp;


@Data
@Entity
@Table(name="FINGER_PRINTS")
public class FingerPrint {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "HAND")
    private String hand; //left or right

    @Column(name="INDEX_FINGER",columnDefinition="TEXT")
    private String indexFinger;

    @Column(name="MIDDLE_FINGER",columnDefinition="TEXT")
    private String middleFinger;

    @Column(name="RING_FINGER",columnDefinition="TEXT")
    private String ringFinger;

    @Column(name="PINKY_FINGER",columnDefinition="TEXT")
    private String pinkyFinger;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "CUSTOMER")
    private Long customer;

    @Column(name = "CUSTOMER_NAME")
    private String customerName;

    @CreationTimestamp
    @Column(name = "CREATED_AT")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "UPDATED_AT")
    private Timestamp updatedAt;
}