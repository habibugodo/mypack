package tz.co.nbc.mypack;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import tz.co.nbc.mypack.cbsClient.CbsClient;
import tz.co.nbc.mypack.cbsClient.CbsClientUtils;
import tz.co.nbc.mypack.enums.CbsTokenType;
import tz.co.nbc.mypack.enums.Role;
import tz.co.nbc.mypack.models.AccountStatus;
import tz.co.nbc.mypack.models.AppStorage;
import tz.co.nbc.mypack.models.User;
import tz.co.nbc.mypack.repositories.AppStorageRepository;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.services.FileStorageService;

import javax.crypto.spec.SecretKeySpec;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static tz.co.nbc.mypack.utils.JwtReader.readToken;


@Component
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class DatabaseSeeder implements CommandLineRunner {


    @NonNull FileStorageService storageService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CbsClientUtils cbsClientUtils;

    private static final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    @Autowired
    private AppStorageRepository appStorageRepository;

    @Override
    public void run(String... args) throws Exception {
//        storageService.deleteAll();
//        storageService.init();
//        System.out.println("Expired At"+readToken(""));
        logger.info("Current Time" + LocalDateTime.now());
        AddAdminUsers();
        logger.info("Current Time" + LocalDateTime.now());
        generateCbsToken();
        logger.info("Current Time" + LocalDateTime.now());
    }


    public void generateCbsToken() {
        AppStorage appStorage = this.appStorageRepository.findByItemKey(CbsTokenType.CBS_TOKEN.name()).orElse(new AppStorage());
        appStorage.setItemKey(CbsTokenType.CBS_TOKEN.name());
        appStorage.setItemDescription("");
        appStorage.setExpiration(LocalDateTime.now().minus(9, ChronoUnit.MINUTES));
        this.appStorageRepository.save(appStorage);
//        cbsClientUtils.Authenticate();
    }

    public void AddAdminUsers() {
        if (userRepository.count() == 0) {
            User user = new User();
            user.setFullName("Godo Habibu");
            user.setEmail("godo@nbc.co.tz");
            user.setPassword(passwordEncoder.encode("Connect@123"));
            user.setPhoneNumber("255787000116");
            user.setStatus(AccountStatus.ACTIVE.name());
            user.setRole(Role.ADMIN.toString());
            User result = userRepository.save(user);
        }
    }


}