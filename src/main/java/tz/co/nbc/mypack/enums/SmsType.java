package tz.co.nbc.mypack.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;

public enum SmsType {
    OTP("OTP"),
    REGISTRATION("REGISTRATION"),
    PASSWORD_RESET("PASSWORD_RESET");

    private final String option;

    SmsType(String option) {
        this.option = option;
    }

    public String getSmsType() {
        return option;
    }

    @JsonCreator
    public static SmsType searchSmsType(final String status) {
        return Stream.of(SmsType.values()).filter(targetEnum -> targetEnum.name().equals(status)).findFirst().orElse(null);
    }
}