package tz.co.nbc.mypack.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;


public enum AppStatus {
    ACTIVE("Active"),
    DISABLED("Disabled");

    private final String status;

    AppStatus(String option) {
        this.status = option;
    }

    public String getStatus() {
        return status;
    }

    @JsonCreator
    public static AppStatus searchGeneralStatus(final String status) {
        return Stream.of(AppStatus.values()).filter(targetEnum -> targetEnum.name().equals(status)).findFirst().orElse(null);
    }
}