package tz.co.nbc.mypack.enums;

public enum SalesDiaryStatus {
    COMPLETED("COMPLETED"),
    NOT_COMPLETED("NOT_COMPLETED");

    private final String option;

    SalesDiaryStatus(String option) {
        this.option = option;
    }

    public String getStatus() {
        return option;
    }

}