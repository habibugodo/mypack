package tz.co.nbc.mypack.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;


public enum Role {
    USER("USER"),
    ADMIN("ADMIN"),
    AGENT("AGENT"),
    BRANCH_MANAGER("BRANCH_MANAGER");

    private final String role;
    Role(String role) {
        this.role=role;
    }

    public String getRole() {
        return role;
    }

    @JsonCreator
    public static Role searchRole(final String status) {
        return Stream.of(Role.values()).filter(targetEnum -> targetEnum.name().equals(status)).findFirst().orElse(null);
    }
}