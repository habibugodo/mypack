package tz.co.nbc.mypack.enums;


public enum CustomerStatus {
    NEW("NEW"),
    APPROVED("APPROVED"),
    PEP("PEP"); //Potentila interest person

    private final String option;

    CustomerStatus(String option) {
        this.option = option;
    }

    public String getStatus() {
        return option;
    }

}