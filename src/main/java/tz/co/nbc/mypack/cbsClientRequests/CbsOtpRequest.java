package tz.co.nbc.mypack.cbsClientRequests;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsOtpRequest {
    @NotBlank
    private String nin;
    @NotBlank
    private String phoneNumber;
}
