package tz.co.nbc.mypack.cbsClientRequests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class HandRequest {

    @NotBlank
    private String indexFinger;

    @NotBlank
    private String middleFinger;

    @NotBlank
    private String ringFinger;

    @NotBlank
    private String pinkyFinger;

}