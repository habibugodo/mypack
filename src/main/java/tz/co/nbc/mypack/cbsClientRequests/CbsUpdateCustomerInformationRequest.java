package tz.co.nbc.mypack.cbsClientRequests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsUpdateCustomerInformationRequest {
    @NotNull
    private String customer;
    @NotNull
    private String currentEmail;
    @NotNull
    private String maritalStatus;
    @NotNull
    private String  title;
    @NotNull
    private String  occupation;
    @NotNull
    private String  jobPosition;
    @NotNull
    private String  employerName;
    @NotNull
    private String  employerAddress;
    @NotNull
    private String  monthlyIncome;
    @NotNull
    private String  educationLevel;
    @NotNull
    private String  spouceName;
    @NotNull
    private String  spoucePhoneNumber;
}