package tz.co.nbc.mypack.cbsClientRequests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsSubmitApplicationRequest {
    @NotNull
    private String customer;
    @NotNull
    private int productId;
    @NotNull
    private int branchId;
    @NotNull
    private String currencyCode;
    @NotNull
    private String needMobileBanking;
    @NotNull
    private String needInternetBanking;
    @NotNull
    private String needHellowMoney;
    @NotNull
    private String needSmsAlert;
    @NotNull
    private String needEStatement;
    @NotNull
    private String needDebitCard;
    @NotNull
    private String description;
}
