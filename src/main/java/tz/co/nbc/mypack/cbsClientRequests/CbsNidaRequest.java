package tz.co.nbc.mypack.cbsClientRequests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsNidaRequest {
    @NotBlank
    private String fingerCode;
    @NotBlank
    private String fingerImage;
    @NotBlank
    private String customer;

//    @NotNull
//    private HandRequest leftHand;
//
//    @NotNull
//    private HandRequest rightHand;
}