package tz.co.nbc.mypack.cbsClientRequests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CbsVerifyOtpRequest {
    @NotBlank
    private String customer;
    @NotBlank
    private String phone;
    @NotBlank
    private String otp;
}
