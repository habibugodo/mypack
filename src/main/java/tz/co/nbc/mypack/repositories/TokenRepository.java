package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.Token;
import java.util.ArrayList;
import java.util.Optional;


@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    Optional<Token> findByToken(String token);

    @Query(value="SELECT * FROM TOKENS WHERE USER_ID=?1 AND (EXPIRED='false' OR REVOKED='false') ",nativeQuery=true)
    ArrayList<Token> findValidTokens(Long userId);

}