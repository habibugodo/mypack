package tz.co.nbc.mypack.repositories.optionsData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.optionsData.BankProduct;
import java.util.Optional;


@Repository
public interface BankProductsRepository extends JpaRepository<BankProduct, Long> {

    Optional<BankProduct> findByNameIgnoreCase(String name);
    @Query(value="SELECT COUNT(ID) FROM BANK_PRODUCTS",nativeQuery=true)
    Long totalProducts();
}