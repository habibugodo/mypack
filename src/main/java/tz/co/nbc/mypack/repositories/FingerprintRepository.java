package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.FingerPrint;


@Repository
public interface FingerprintRepository extends JpaRepository<FingerPrint, Long> {

}