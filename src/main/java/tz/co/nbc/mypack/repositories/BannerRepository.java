package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.Banner;


@Repository
public interface BannerRepository extends JpaRepository<Banner, Long> {

}