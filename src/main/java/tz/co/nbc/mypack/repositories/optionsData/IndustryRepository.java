package tz.co.nbc.mypack.repositories.optionsData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.optionsData.Industry;


import java.util.Optional;


@Repository
public interface IndustryRepository extends JpaRepository<Industry, Long> {

    Optional<Industry> findByNameIgnoreCase(String name);
}