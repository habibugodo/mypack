package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.Department;


@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

}