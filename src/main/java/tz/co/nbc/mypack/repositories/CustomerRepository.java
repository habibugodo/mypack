package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.Customer;
import tz.co.nbc.mypack.models.SalesDiary;

import java.util.List;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query(value="SELECT COUNT(ID) FROM CUSTOMER",nativeQuery=true)
    Long totalCustomer();

    @Query(value="SELECT COUNT(ID) FROM CUSTOMER WHERE AGENT_ID=?1",nativeQuery=true)
    Long totalCustomerByAgent(Long agentId);

    @Query(value="SELECT COUNT(ID) FROM CUSTOMER WHERE AGENT_ID=?1 AND IS_PEP=?2",nativeQuery=true)
    Long totalCustomerByAgentIsPEP(Long agentId,boolean isPEP);

    @Query(value="SELECT COUNT(ID) FROM CUSTOMER WHERE AGENT_ID=?1 AND STATUS=?2",nativeQuery=true)
    Long totalCustomerByAgentAndApproved(Long agentId,String status);


    @Query(value="SELECT * FROM CUSTOMER WHERE AGENT_ID=?1 ORDER BY ID DESC",nativeQuery=true)
    List<Customer> getCustomerByAgent(Long agentId);
}