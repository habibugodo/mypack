package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.exceptions.ResourceNotFoundException;
import tz.co.nbc.mypack.models.User;
import tz.co.nbc.mypack.security.UserPrincipal;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findById(Long id);

    Optional<User> findByEmail(@NotBlank String email);

//    Optional<User> findByUsername(@NotBlank String email);

//    Optional<User> findByUsernameOrEmail(@NotBlank String username,@NotBlank String email);
//    Optional<User> findByEmailOrUsername(@NotBlank String email,@NotBlank String username);

    Optional<User> findByPhoneNumber(@NotBlank String phone);

    Optional<User> findByRefreshToken(@NotBlank String token);

    Optional<User> findByPasswordResetToken(@NotBlank String token);

    @Query(value="SELECT * FROM USERS WHERE COMPANY_ID=?1",nativeQuery=true)
    Optional<User> getAllUserByCompany(Long companyId);

    @Query(value="SELECT * FROM USERS WHERE ROLE=?1 ORDER BY ID DESC",nativeQuery=true)
    List<User> getAgents(String role);

    @Query(value="SELECT COUNT(ID) FROM USERS WHERE ROLE=?1",nativeQuery=true)
    Long totalAgents(String role);


    @Query(value="SELECT * FROM USERS WHERE COMPANY_ID=?1",nativeQuery=true)
    List<User> getUserByCompanyId(Long companyId);

    Boolean existsByEmail(@NotBlank String email);

    Boolean existsByPhoneNumber(@NotBlank String phone);

//    Optional<User> findByUsernameOrEmail(String username, String email);

    default User getUser(UserPrincipal currentUser) {
        return getUserByEmail(currentUser.getUsername());
    }

    default User getUserByEmail(String email) {
        return findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("Email", "email", email));
    }

    @Query(value="SELECT COUNT(ID) FROM USERS",nativeQuery=true)
    Integer totalUsers();
}