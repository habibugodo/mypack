package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.Branch;


@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {


    @Query(value="SELECT COUNT(ID) FROM BRANCH",nativeQuery=true)
    Long totalBranches();

}