package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.AppStorage;
import tz.co.nbc.mypack.models.Customer;
import tz.co.nbc.mypack.models.TemporaryCustomer;

import java.util.List;
import java.util.Optional;


@Repository
public interface TemporaryCustomerRepository extends JpaRepository<TemporaryCustomer, Long> {

    Optional<TemporaryCustomer> findByCustomerId(String customer);

    @Query(value="SELECT * FROM CUSTOMER WHERE AGENT=?1 ORDER BY ID DESC",nativeQuery=true)
    List<TemporaryCustomer> getCustomerByAgent(Long agentId);
}