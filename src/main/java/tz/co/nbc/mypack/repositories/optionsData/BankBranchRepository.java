package tz.co.nbc.mypack.repositories.optionsData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.optionsData.BankBranch;
import java.util.Optional;


@Repository
public interface BankBranchRepository extends JpaRepository<BankBranch, Long> {

    Optional<BankBranch> findByNameIgnoreCase(String name);

    @Query(value="SELECT COUNT(ID) FROM BANK_BRANCHES",nativeQuery=true)
    Long totalBranches();

}