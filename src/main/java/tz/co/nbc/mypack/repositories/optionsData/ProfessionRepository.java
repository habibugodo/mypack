package tz.co.nbc.mypack.repositories.optionsData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.optionsData.Profession;


import java.util.Optional;


@Repository
public interface ProfessionRepository extends JpaRepository<Profession, Long> {

    Optional<Profession> findByNameIgnoreCase(String name);
}