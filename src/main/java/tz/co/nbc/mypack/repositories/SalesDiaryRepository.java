package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.SalesDiary;


import java.util.List;


@Repository
public interface SalesDiaryRepository extends JpaRepository<SalesDiary, Long> {

    @Query(value="SELECT COUNT(ID) FROM SALES_DIARIES",nativeQuery=true)
    Long totalSalesDiaries();

    @Query(value="SELECT COUNT(ID) FROM SALES_DIARIES WHERE AGENT_ID=?1",nativeQuery=true)
    Long totalSalesDiariesByAgent(Long agentId);

    @Query(value="SELECT COUNT(ID) FROM SALES_DIARIES WHERE AGENT_ID=?1 AND NEXT_APPOINTMENT=?2 ",nativeQuery=true)
    Long totalSalesDiariesByAgentWithAppointment(Long agentId,String nextAppointment);

    @Query(value="SELECT * FROM SALES_DIARIES WHERE AGENT_ID=?1",nativeQuery=true)
    List<SalesDiary> getDiariesByAgent(Long agentId);

    @Query(value="SELECT * FROM SALES_DIARIES WHERE DIARY_TOPIC_ID=?1",nativeQuery=true)
    List<SalesDiary> getDiariesByDiaryTopic(Long topicId);

    @Query(value="SELECT * FROM SALES_DIARIES WHERE BRANCH_ID=?1",nativeQuery=true)
    List<SalesDiary> getDiariesByBranch(Long branchId);

}