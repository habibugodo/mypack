package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.SmsLog;


import java.util.List;


@Repository
public interface SmsLogsRepository extends JpaRepository<SmsLog, Long> {

    //Order by Id
    @Query(value="SELECT * FROM SMS_LOGS WHERE PHONE_NUMBER=?1",nativeQuery=true)
    List<SmsLog> getByPhoneNumber(String phoneNumber);

    @Query(value="SELECT * FROM SMS_LOGS WHERE SMS_TYPE=?1",nativeQuery=true)
    List<SmsLog> getBySmsType(String smsType);
}