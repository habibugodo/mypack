package tz.co.nbc.mypack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.AppStorage;

import java.util.Optional;


@Repository
public interface AppStorageRepository extends JpaRepository<AppStorage, Long> {

    Optional<AppStorage> findByItemKey(String key);

}