package tz.co.nbc.mypack.repositories.optionsData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tz.co.nbc.mypack.models.optionsData.SourceOfFunds;


import java.util.Optional;


@Repository
public interface SourceOfFundsRepository extends JpaRepository<SourceOfFunds, Long> {
    Optional<SourceOfFunds> findByNameIgnoreCase(String name);

}