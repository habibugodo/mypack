package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.enums.CustomerStatus;
import tz.co.nbc.mypack.enums.Role;
import tz.co.nbc.mypack.enums.SalesDiaryStatus;
import tz.co.nbc.mypack.payloads.dashboard.AdminDashboardStats;
import tz.co.nbc.mypack.payloads.dashboard.AgentDashboardStats;
import tz.co.nbc.mypack.repositories.BranchRepository;
import tz.co.nbc.mypack.repositories.CustomerRepository;
import tz.co.nbc.mypack.repositories.SalesDiaryRepository;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.repositories.optionsData.BankBranchRepository;
import tz.co.nbc.mypack.repositories.optionsData.BankProductsRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;


@Controller
@RequestMapping(Constants.API_V1 + "/dashboard")
public class DashboardController {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private CustomerRepository customerRepository;


    @Autowired
    private BankProductsRepository bankProductsRepository;

    @Autowired
    private BankBranchRepository bankBranchRepository;


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SalesDiaryRepository salesDiaryRepository;

    @GetMapping("/admin-stats")
    public ResponseEntity<?> getAdminStats(){
        AdminDashboardStats dashboardStats =new AdminDashboardStats();
        dashboardStats.setTotalBranches(bankBranchRepository.totalBranches());
        dashboardStats.setTotalCustomers(customerRepository.totalCustomer());
        dashboardStats.setTotalAgents(userRepository.totalAgents(Role.AGENT.getRole()));
//        dashboardStats.setTotalCardTypes(cardTypeRepository.totalCardTypes());
        dashboardStats.setTotalProducts(bankProductsRepository.totalProducts());
        dashboardStats.setTotalSalesDiaries(salesDiaryRepository.totalSalesDiaries());
        return new ResponseEntity<>(dashboardStats, HttpStatus.OK);
    }

    @GetMapping("/agent-stats")
    public ResponseEntity<?> getAgents(UserPrincipal currentUser){
        AgentDashboardStats agentDashboardStats=new AgentDashboardStats();
        agentDashboardStats.setTotalOpenedAccounts(this.customerRepository.totalCustomerByAgent(currentUser.getId()));
        agentDashboardStats.setTotalPEPAccounts(this.customerRepository.totalCustomerByAgentIsPEP(currentUser.getId(),true));
        agentDashboardStats.setSuccessfullyOpenedAccounts(this.customerRepository.totalCustomerByAgentAndApproved(currentUser.getId(), CustomerStatus.APPROVED.getStatus()));
        agentDashboardStats.setTotalSalesDiaries(salesDiaryRepository.totalSalesDiariesByAgent(currentUser.getId()));
        agentDashboardStats.setTotalNextAppointments(salesDiaryRepository.totalSalesDiariesByAgentWithAppointment(currentUser.getId(), SalesDiaryStatus.NOT_COMPLETED.getStatus()));
        return new ResponseEntity<>(agentDashboardStats, HttpStatus.OK);
    }
}
