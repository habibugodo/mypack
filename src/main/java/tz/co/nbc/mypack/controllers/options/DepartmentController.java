package tz.co.nbc.mypack.controllers.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.Department;
import tz.co.nbc.mypack.models.ItemReqeust;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.DepartmentRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;
import javax.validation.Valid;
import java.util.Optional;


@Controller
@RequestMapping(Constants.API_V1 + "/departments")
public class DepartmentController {

    @Autowired
    private DepartmentRepository departmentRepository;

    @GetMapping
    public ResponseEntity<?> getDepartments(){
        return new ResponseEntity<>(this.departmentRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDepartment(@PathVariable(value ="id") long id){
        Optional<Department> department=this.departmentRepository.findById(id);
        if(department.isEmpty())
            return new ResponseEntity<>(new Message("Department with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(department.get(),HttpStatus.OK);
    }



    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> getDepartments(UserPrincipal currentUser, @Valid @RequestBody ItemReqeust departmentRequest){
        Department department=new Department();
        department.setName(departmentRequest.getName());
        department.setCreatedBy(currentUser.getId());
        department.setCreatedByName(currentUser.getFullName());
        return  new ResponseEntity<>(this.departmentRepository.save(department),HttpStatus.CREATED);
    }


    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> updateDepartments(UserPrincipal currentUser, @Valid @RequestBody ItemReqeust departmentRequest){
        Optional<Department> department=this.departmentRepository.findById(departmentRequest.getId());
        if(department.isEmpty())
            return new ResponseEntity<>(new Message("Department with id "+departmentRequest.getId()+" not found"),HttpStatus.BAD_REQUEST);
        department.get().setName(departmentRequest.getName());
        return  new ResponseEntity<>(this.departmentRepository.save(department.get()),HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteDepartments(@PathVariable(value ="id") long id){
        Optional<Department> department=this.departmentRepository.findById(id);
        if(department.isEmpty())
            return new ResponseEntity<>(new Message("Department with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.departmentRepository.delete(department.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}