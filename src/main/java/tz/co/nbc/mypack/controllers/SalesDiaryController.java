package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.enums.Role;
import tz.co.nbc.mypack.enums.SalesDiaryStatus;
import tz.co.nbc.mypack.models.Branch;
import tz.co.nbc.mypack.models.SalesDiary;
import tz.co.nbc.mypack.models.SalesDiaryTopic;
import tz.co.nbc.mypack.models.User;
import tz.co.nbc.mypack.payloads.SalesDiaryRequest;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.BranchRepository;
import tz.co.nbc.mypack.repositories.SalesDiaryRepository;
import tz.co.nbc.mypack.repositories.SalesDiaryTopicRepository;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;
import tz.co.nbc.mypack.utils.PhoneUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/sales-diaries")
public class SalesDiaryController {

    //Get sales diary with next appointments
    //Update sales diary appointment to show completed

    @Autowired
    private SalesDiaryRepository salesDiaryRepository;

    @Autowired
    private SalesDiaryTopicRepository salesDiaryTopicRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BranchRepository branchRepository;

    @GetMapping
    public ResponseEntity<?> getSalesDiaries(){
        return new ResponseEntity<>(this.salesDiaryRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSalesDiary(@PathVariable(value ="id") long id){
        Optional<SalesDiary> salesDiary=this.salesDiaryRepository.findById(id);
        if(salesDiary.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(salesDiary.get(),HttpStatus.OK);
    }

    //Get sales diary by agent
    @GetMapping("/byAgent/{id}")
    public ResponseEntity<?> getSalesDiariesByAgent(@PathVariable(value ="id") long id){
        Optional<User> user=this.userRepository.findById(id);
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+id+" not found"),HttpStatus.BAD_REQUEST);
        if(user.get().getRole().equalsIgnoreCase(Role.AGENT.getRole())){
            return new ResponseEntity<>(new Message("User provided is not an agent"),HttpStatus.BAD_REQUEST);
        }
        List<SalesDiary> salesDiaries=this.salesDiaryRepository.getDiariesByAgent(id);
        return new ResponseEntity<>(salesDiaries,HttpStatus.OK);
    }

    //Get sales diary by branch
    @GetMapping("/byBranch/{id}")
    public ResponseEntity<?> getSalesDiariesByBranch(@PathVariable(value ="id") long id){
        Optional<Branch> branch=this.branchRepository.findById(id);
        if(branch.isEmpty())
            return new ResponseEntity<>(new Message("Branch with "+id+" not found"),HttpStatus.BAD_REQUEST);
        List<SalesDiary> salesDiaries=this.salesDiaryRepository.getDiariesByBranch(branch.get().getId());
        return new ResponseEntity<>(salesDiaries,HttpStatus.OK);
    }

    //Get sales diary by topic
    @GetMapping("/byTopic/{id}")
    public ResponseEntity<?> getSalesDiariesByTopic(@PathVariable(value ="id") long id){
        Optional<SalesDiaryTopic> salesDiaryTopic=this.salesDiaryTopicRepository.findById(id);
        if(salesDiaryTopic.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary topic id "+id+" not found"),HttpStatus.BAD_REQUEST);
        List<SalesDiary> salesDiaries=this.salesDiaryRepository.getDiariesByDiaryTopic(salesDiaryTopic.get().getId());
        return new ResponseEntity<>(salesDiaries,HttpStatus.OK);
    }

    //Get sales diary with next appointments



    @PostMapping
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addDiaryTopic(UserPrincipal currentUser, @Valid @RequestBody SalesDiaryRequest salesDiaryRequest){

        Optional<User> user=this.userRepository.findById(currentUser.getId());
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+currentUser.getId()+" not found"),HttpStatus.BAD_REQUEST);
//        if(user.get().getRole().equalsIgnoreCase(Role.AGENT.getRole())){
//            return new ResponseEntity<>(new Message("User provided is not an agent"),HttpStatus.BAD_REQUEST);
//        }

        Optional<SalesDiaryTopic> salesDiaryTopic=this.salesDiaryTopicRepository.findById(salesDiaryRequest.getTopicId());
        if(salesDiaryTopic.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary topic id "+salesDiaryRequest.getTopicId()+" not found"),HttpStatus.BAD_REQUEST);


        SalesDiary salesDiary=new SalesDiary();
        salesDiary.setTopic(salesDiaryTopic.get().getName());
        salesDiary.setTopicId(salesDiaryTopic.get().getId());
        salesDiary.setDescription(salesDiaryRequest.getDescription());
        salesDiary.setCustomerName(salesDiaryRequest.getCustomerName());
        salesDiary.setEmail(salesDiaryRequest.getEmail());
        //Individual,Company,Organization
        salesDiary.setCustomerType(salesDiaryRequest.getCustomerType());
        String phoneNumber= PhoneUtils.getMobileNumberWithCodeNoPlus(salesDiaryRequest.getPhoneNumber());
        salesDiary.setPhoneNumber(phoneNumber);
        salesDiary.setNumberOfPotentialCustomer(salesDiaryRequest.getNumberOfPotentialCustomer());
        salesDiary.setRating(salesDiaryRequest.getRating());
        salesDiary.setLocation(salesDiaryRequest.getLocation());
        salesDiary.setAddress(salesDiaryRequest.getAddress());
        salesDiary.setHasNextAppointment(salesDiaryRequest.getHasNextAppointment());
        salesDiary.setNextAppointment(salesDiaryRequest.getNextAppointment());
        salesDiary.setAgentId(user.get().getId());
        salesDiary.setAgentName(user.get().getFullName());
        salesDiary.setBranch(user.get().getBranch());
        salesDiary.setBranchName(user.get().getBranchName());
        if(salesDiaryRequest.getHasNextAppointment()){
            salesDiary.setStatus(SalesDiaryStatus.NOT_COMPLETED.getStatus());
        }else {
            salesDiary.setStatus(SalesDiaryStatus.COMPLETED.getStatus());;
        }
        return  new ResponseEntity<>(this.salesDiaryRepository.save(salesDiary),HttpStatus.CREATED);
    }


//    @PutMapping
//    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<?> updateDiaryTopic(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SalesDiaryTopicRequest salesDiaryTopicRequest){
//        Optional<SalesDiaryTopic> salesDiaryTopic=this.salesDiaryTopicRepository.findById(salesDiaryTopicRequest.getId());
//        if(salesDiaryTopic.isEmpty())
//            return new ResponseEntity<>(new Message("Sales diary topic id "+salesDiaryTopicRequest.getId()+" not found"),HttpStatus.BAD_REQUEST);
//        salesDiaryTopic.get().setName(salesDiaryTopicRequest.getName());
//        return  new ResponseEntity<>(this.salesDiaryTopicRepository.save(salesDiaryTopic.get()),HttpStatus.OK);
//    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteDiaryTopic(@PathVariable(value ="id") long id){
        Optional<SalesDiary> salesDiary=this.salesDiaryRepository.findById(id);
        if(salesDiary.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.salesDiaryRepository.delete(salesDiary.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}