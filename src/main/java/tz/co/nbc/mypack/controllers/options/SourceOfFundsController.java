package tz.co.nbc.mypack.controllers.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.ItemReqeust;
import tz.co.nbc.mypack.models.optionsData.SourceOfFunds;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.optionsData.SourceOfFundsRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@Controller
@RequestMapping(Constants.API_V1 + "/source-of-funds")
public class SourceOfFundsController {

    @Autowired
    private SourceOfFundsRepository sourceOfFundsRepository;

    @GetMapping
    public ResponseEntity<?> getSourceOfFunds(){
        return new ResponseEntity<>(this.sourceOfFundsRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSourceOfFund(@PathVariable(value ="id") long id){
        Optional<SourceOfFunds> sourceOfFund=this.sourceOfFundsRepository.findById(id);
        if(sourceOfFund.isEmpty())
            return new ResponseEntity<>(new Message("Source of funds with "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(sourceOfFund.get(),HttpStatus.OK);
    }


    @PostMapping("/bulk")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addSourceOfFundsBulk(@RequestBody List<String> stringList){
        for (String item:stringList) {
            SourceOfFunds sourceOfFunds=this.sourceOfFundsRepository.findByNameIgnoreCase(item).orElse(new SourceOfFunds());
            sourceOfFunds.setName(item);
            this.sourceOfFundsRepository.save(sourceOfFunds);
        }
        return new ResponseEntity<>(new Message("Saved batch successfully"),HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addSourceOfFunds(UserPrincipal currentUser, @Valid @RequestBody ItemReqeust itemReqeust){
        Optional<SourceOfFunds> exists=this.sourceOfFundsRepository.findByNameIgnoreCase(itemReqeust.getName());
        if(exists.isPresent()){
            return new ResponseEntity<>(new Message("Source of funds "+itemReqeust.getName()+" already exist."),HttpStatus.OK);
        }
        SourceOfFunds sourceOfFunds=new SourceOfFunds();
        sourceOfFunds.setName(itemReqeust.getName());
        return  new ResponseEntity<>(this.sourceOfFundsRepository.save(sourceOfFunds),HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteSourceOfFund(@PathVariable(value ="id") long id){
        Optional<SourceOfFunds> sourceOfFunds=this.sourceOfFundsRepository.findById(id);
        if(sourceOfFunds.isEmpty())
            return new ResponseEntity<>(new Message("Source of funds with "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.sourceOfFundsRepository.delete(sourceOfFunds.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
