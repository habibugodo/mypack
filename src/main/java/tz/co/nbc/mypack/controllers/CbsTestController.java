package tz.co.nbc.mypack.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.cbs.nida.NidaFingerprintRequest;
import tz.co.nbc.mypack.cbsClient.CbsClient;
import tz.co.nbc.mypack.cbsClient.CbsClientRequest;
import tz.co.nbc.mypack.cbsClient.CbsClientResponse;
import tz.co.nbc.mypack.cbsClientRequests.CbsOtpRequest;
import tz.co.nbc.mypack.cbsClientRequests.CbsVerifyOtpRequest;
import tz.co.nbc.mypack.cbsClientResponses.authorization.Authorization;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.CbsTokenType;
import tz.co.nbc.mypack.payloads.generic.ErrorResponse;
import tz.co.nbc.mypack.repositories.optionsData.*;
import tz.co.nbc.mypack.utils.ApiHttpClient;
import tz.co.nbc.mypack.utils.Constants;
import javax.validation.Valid;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

@Controller
@RequestMapping(Constants.API_V1 + "/cbs-test")
public class CbsTestController {

    @Autowired
    private ApiHttpClient apiHttpClient;

    @Autowired
    private SourceOfFundsRepository sourceOfFundsRepository;

    @Autowired
    private IndustryRepository industryRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @Autowired
    private BankBranchRepository bankBranchRepository;

    @Autowired
    private BankProductsRepository bankProductsRepository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private CbsClient cbsClient;

    static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    String API_URL="https://nacbe-v001-nbc-alternative-channels.apps.ocp4-kawe.tz.af.absa.local";


    @GetMapping("/sign-in")
    public ResponseEntity<?> getAuthorizationToken(){
        try {
            String url="https://nacbe-v001-nbc-alternative-channels.apps.ocp4-kawe.tz.af.absa.local/api/v1/Authentication/Portal/Vendors/Authenticate";
            Authorization.Request authorizationRequest=new Authorization.Request("NBCMYPACK","NbcMyPack@2023!");
            String body=gson.toJson(authorizationRequest);
            CompletableFuture<HttpResponse<String>> response=apiHttpClient.GetAuthorizationToken(url,body);
            if (response.get().statusCode() == 200) {
                return new ResponseEntity<>(response.get().body(), HttpStatus.OK);
                //Save Token in appConfig
                //Save products
            }else {
                return new ResponseEntity<>(new ErrorResponse("UNABLE_OBTAIN_TOKEN","Unable to perform authorization"),HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse("UNABLE_OBTAIN_TOKEN","Unable to perform authorization"),HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> customerRegistration(@Valid @RequestBody CbsOtpRequest cbsOtpRequest){
        try {
            String url = API_URL+ "/Customers/Vendors/TemporaryRegister";
            //Get Token from app config
            String body=gson.toJson(cbsOtpRequest);
            CompletableFuture<HttpResponse<String>> response=apiHttpClient.RegisterNewCustomer(url,body,"");
            if (response.get().statusCode() == 200) {
                return new ResponseEntity<>(response.get().body(),HttpStatus.OK);
                //Save Customer is new Customer or not
            }else {
                return new ResponseEntity<>(new ErrorResponse("REGISTRATION_FAILED","Unable to perform registration"),HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse("REGISTRATION_FAILED","Unable to perform registration"),HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/verify-otp")
    public ResponseEntity<?> customerVerifyOtp(@Valid @RequestBody CbsVerifyOtpRequest verifyOtpRequest){
        try {
            //Get Token from app config
            String url;
            boolean isNewCustomer=false;
            if(isNewCustomer){
                //Existing customer
                url = API_URL + "/Customers/Vendors/VerifyMobileNumber";
            }else {
                //New Customer
                url = API_URL+ "/Customers/Vendors/VerifyMobileNumberForTemporaryCustomer";
            }
            String body=gson.toJson(verifyOtpRequest);
            CompletableFuture<HttpResponse<String>> response=apiHttpClient.verifyMobileNumber(url,body,"");
            if (response.get().statusCode() == 200) {
                return new ResponseEntity<>(response.get().body(),HttpStatus.OK);
            }else {
                return new ResponseEntity<>(new ErrorResponse("OTP_VERIFICATION_FAILED","Unable to verify otp"),HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse("OTP_VERIFICATION_FAILED","Unable to verify otp"),HttpStatus.BAD_REQUEST);
        }
    }




    @GetMapping("/update-customer-details-and-application")
    public ResponseEntity<?> updateCustomerDetailsAndApplication(){
        try {
            //Get Token from app config
            String url=API_URL+ "Customers/Vendors/GetUpdatedCustomerAndApplication";
            CompletableFuture<HttpResponse<String>> response=apiHttpClient.UpdateCustomerInformationAndApplication(url,"");
            if (response.get().statusCode() == 200) {
                return new ResponseEntity<>(response.get().body(),HttpStatus.OK);
            }else {
                return new ResponseEntity<>(new ErrorResponse("UPDATE_CUSTOMER_DETAILS_FAILED","Unable to update customer details"),HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse("UPDATE_CUSTOMER_DETAILS_FAILED","Unable to update customer details"),HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/nida-fingerprint")
    public ResponseEntity<?> nidaFingerprint(@Valid @RequestBody NidaFingerprintRequest nidaFingerpringRequest){
        try {
            //Get Token from app config
            String url=API_URL+ "/Customers/KYCQuerying/Vendors/NIDA/Fingerprint";
            String body=gson.toJson(nidaFingerpringRequest);
            CompletableFuture<HttpResponse<String>> response=apiHttpClient.NidaKycQuery(url,body,"");
            if (response.get().statusCode() == 200) {
                return new ResponseEntity<>(response.get().body(),HttpStatus.OK);
            }else {
                return new ResponseEntity<>(new ErrorResponse("UPDATE_CUSTOMER_DETAILS_FAILED","Unable to update customer details"),HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse("UPDATE_CUSTOMER_DETAILS_FAILED","Unable to update customer details\""),HttpStatus.BAD_REQUEST);
        }
    }






    @GetMapping("/source-of-funds")
    public ResponseEntity<?> sourceOfFunds(){
        return new ResponseEntity<>(this.sourceOfFundsRepository.findAll(),HttpStatus.OK);
    }

    @GetMapping("/industries")
    public ResponseEntity<?> industries(){
        return new ResponseEntity<>(this.industryRepository.findAll(),HttpStatus.OK);
    }

    @GetMapping("/professions")
    public ResponseEntity<?> professions(){
        return new ResponseEntity<>(this.professionRepository.findAll(),HttpStatus.OK);
    }

    @GetMapping("/products")
    public ResponseEntity<?> products(){
        return new ResponseEntity<>(this.bankProductsRepository.findAll(),HttpStatus.OK);
    }

    @GetMapping("/bank-branches")
    public ResponseEntity<?> bankBranches(){
        return new ResponseEntity<>(this.bankBranchRepository.findAll(),HttpStatus.OK);
    }

}