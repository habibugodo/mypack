package tz.co.nbc.mypack.controllers.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.optionsData.BankProduct;
import tz.co.nbc.mypack.payloads.BankProductRequest;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.optionsData.BankProductsRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/bank-products")
public class BankProductsController {

    @Autowired
    private BankProductsRepository bankProductsRepository;

    @GetMapping
    public ResponseEntity<?> getBankProducts(){
        return new ResponseEntity<>(this.bankProductsRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBankProduct(@PathVariable(value ="id") long id){
        Optional<BankProduct> bankProducts=this.bankProductsRepository.findById(id);
        if(bankProducts.isEmpty())
            return new ResponseEntity<>(new Message("Bank product with "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(bankProducts.get(),HttpStatus.OK);
    }


    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addBankProduct(UserPrincipal currentUser, @Valid @RequestBody BankProductRequest bankProductRequest){
        Optional<BankProduct> exists=this.bankProductsRepository.findByNameIgnoreCase(bankProductRequest.getName());
        if(exists.isPresent()){
            return new ResponseEntity<>(new Message("Bank product  "+bankProductRequest.getName()+" already exist."),HttpStatus.BAD_REQUEST);
        }
        BankProduct bankProducts=new BankProduct();
        bankProducts.setName(bankProductRequest.getName());
        bankProducts.setProductId(bankProductRequest.getProductId());
        return  new ResponseEntity<>(this.bankProductsRepository.save(bankProducts),HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteSourceOfFund(@PathVariable(value ="id") long id){
        Optional<BankProduct> bankProducts=this.bankProductsRepository.findById(id);
        if(bankProducts.isEmpty())
            return new ResponseEntity<>(new Message("Bank products with "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.bankProductsRepository.delete(bankProducts.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
