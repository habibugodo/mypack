package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.enums.SmsType;
import tz.co.nbc.mypack.models.SmsLog;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.SmsLogsRepository;
import tz.co.nbc.mypack.utils.Constants;
import java.util.List;
import java.util.Optional;


@Controller
@RequestMapping(Constants.API_V1 + "/sms-logs")
public class SmsLogController {

    @Autowired
    private SmsLogsRepository smsLogsRepository;

    @GetMapping
    public ResponseEntity<?> getSmsLogs() {
        return new ResponseEntity<>(this.smsLogsRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSmsLog(@PathVariable(value = "id") long id) {
        Optional<SmsLog> smsLog = this.smsLogsRepository.findById(id);
        if (smsLog.isEmpty())
            return new ResponseEntity<>(new Message("Sms log with " + id + " not found"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(smsLog.get(), HttpStatus.OK);
    }

    @GetMapping("byPhoneNumber/{phoneNumber}")
    public ResponseEntity<?> getSmsByPhoneNumber(@PathVariable(value = "phoneNumber") String phoneNumber) {
        List<SmsLog> smsLogs = this.smsLogsRepository.getByPhoneNumber(phoneNumber);
        return new ResponseEntity<>(smsLogs, HttpStatus.OK);
    }

    @GetMapping("bySmsType/{type}")
    public ResponseEntity<?> getSmsBySmsType(@PathVariable(value = "type") String type) {
        SmsType smsType=SmsType.searchSmsType(type);
        if(smsType==null){
            return new ResponseEntity<>(new Message("Sms type" + type + " not supported"), HttpStatus.BAD_REQUEST);
        }
        List<SmsLog> smsLogs = this.smsLogsRepository.getBySmsType(type);
        return new ResponseEntity<>(smsLogs, HttpStatus.OK);

    }
}