package tz.co.nbc.mypack.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.cbsClient.CbsSmsUtils;
import tz.co.nbc.mypack.cbsClientResponses.login.LoginOtp;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.Role;
import tz.co.nbc.mypack.enums.SmsType;
import tz.co.nbc.mypack.enums.TokenType;
import tz.co.nbc.mypack.models.AccountStatus;
import tz.co.nbc.mypack.models.Token;
import tz.co.nbc.mypack.models.User;
import tz.co.nbc.mypack.payloads.auth.*;
import tz.co.nbc.mypack.payloads.generic.ErrorResponse;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.payloads.generic.SuccessResponse;
import tz.co.nbc.mypack.repositories.TokenRepository;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.security.JwtResponse;
import tz.co.nbc.mypack.security.JwtTokenProvider;
import tz.co.nbc.mypack.services.OtpService;
import tz.co.nbc.mypack.utils.Constants;
import tz.co.nbc.mypack.utils.PhoneUtils;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import static tz.co.nbc.mypack.utils.DateUtils.LocalDateTimeFormatter;
import static tz.co.nbc.mypack.utils.PhoneUtils.getMobileNumberNbcFormat;


@Controller
@RequestMapping(Constants.API_V1 + "/auth")
public class AuthController {

    Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private OtpService otpService;

    @Autowired
    private CbsSmsUtils cbsSmsUtils;

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new Message("Email is already taken"), HttpStatus.BAD_REQUEST);
        }
        String phoneNumber = PhoneUtils.getMobileNumberWithCodeNoPlus(signUpRequest.getPhoneNumber());
        if (userRepository.existsByPhoneNumber(phoneNumber)) {
            return new ResponseEntity<>(new Message("Phone is already taken"), HttpStatus.BAD_REQUEST);
        }
        if (!signUpRequest.getPassword().equals(signUpRequest.getConfirmPassword())) {
            return new ResponseEntity<>(new Message("Password mismatch"), HttpStatus.BAD_REQUEST);
        }
        String password = passwordEncoder.encode(signUpRequest.getPassword());


        User user = new User();
        user.setFullName(signUpRequest.getFullName());
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(password);
        user.setPhoneNumber(phoneNumber);
        user.setStatus(AccountStatus.ACTIVE.name());
        user.setRole(Role.ADMIN.toString());
        User result = userRepository.save(user);

        //@Todo activate account after email verification
        SuccessResponse successResponse=new SuccessResponse();
        successResponse.setSuccess("ADMIN_REGISTER_SUCCESS");
        successResponse.setSuccessDescription("Admin User registered successfully");
        return new ResponseEntity<>(successResponse,HttpStatus.OK);
    }


    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        JwtResponse jwtResponse = jwtTokenProvider.generateToken(authentication);
        Optional<User> user = userRepository.findByEmail(loginRequest.getEmail());
        if (user.isEmpty()) {
            logger.info("User {}", user);
            return new ResponseEntity<>(new ErrorResponse("INVALID_USERNAME_OR_PASSWORD","Invalid Username and Password","400"),HttpStatus.BAD_REQUEST);
        }

        String refreshToken = UUID.randomUUID().toString();
        user.get().setRefreshToken(refreshToken);
        LocalDateTime tokenExpiration = LocalDateTime.now().plus(appConfig.getREFRESH_TOKEN_EXPIRATION(), ChronoUnit.MINUTES);
        user.get().setRefreshTokenExpiration(tokenExpiration);
        user.get().setLastLogin(String.valueOf(LocalDateTime.now()));
        User savedUser=this.userRepository.save(user.get());

        if(!user.get().isMultiDeviceLogin()){
            revokeToken(savedUser);
        }
        saveToken(savedUser,jwtResponse.getToken(),jwtResponse.getTokenExpiration());
        LoginResponse.Response loginResponse = new LoginResponse.Response();
        loginResponse.setUser(getUserResponse(user.get()));
        loginResponse.setTokenType("Bearer");
        loginResponse.setToken(jwtResponse.getToken());
        loginResponse.setTokenExpiration(LocalDateTimeFormatter(jwtResponse.getTokenExpiration()));
        loginResponse.setRefreshToken(refreshToken);
        return ResponseEntity.ok(loginResponse);
    }


    @PostMapping("/mobile-login")
    public ResponseEntity<?> mobileLogin(@Valid @RequestBody LoginRequest loginRequest) throws IOException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        JwtResponse jwtResponse = jwtTokenProvider.generateToken(authentication);
        Optional<User> user = userRepository.findByEmail(loginRequest.getEmail());
        if (user.isEmpty()) {
            logger.info("User {}", user);
            return new ResponseEntity<>(new ErrorResponse("INVALID_USERNAME_OR_PASSWORD","Invalid Username and Password","400"),HttpStatus.BAD_REQUEST);
        }


//        ArrayList<String> phoneNumber = new ArrayList<>();
//        phoneNumber.add(user.get().getPhoneNumber());
        String otp = otpService.generateToken();
//        String message = "Your otp token is " + otp + ". Never share this code with anyone including us.";
//        otpService.sendOTPSms(phoneNumber, message);

        LoginOtp.Request request=new LoginOtp.Request();
        request.setMobile(getMobileNumberNbcFormat(user.get().getPhoneNumber()));
        request.setName("Agent");
        request.setOtp(otp);
        request.setPreferred_language("EN");
        logger.info("Otp Generation Request: "+request);
        if(cbsSmsUtils.sendSms(request)){
            logger.info("Otp sms sent successfully");
        }else {
            logger.info("OTP sending failed");
        }

        user.get().setOtpToken(otp);
        LocalDateTime otpTokenExpiration = LocalDateTime.now().plus(1, ChronoUnit.DAYS);
        user.get().setOtpTokenExpiration(otpTokenExpiration);


        User savedUser=this.userRepository.save(user.get());
        if(!user.get().isMultiDeviceLogin()){
            revokeToken(savedUser);
        }

        TwoFactorResponse twoFactorResponse = new TwoFactorResponse();
        twoFactorResponse.setUserId(user.get().getId());
        twoFactorResponse.setPhoneNumber(user.get().getPhoneNumber());
        twoFactorResponse.setMessage("Otp token has been sent to " + user.get().getPhoneNumber());
        return ResponseEntity.ok(twoFactorResponse);
    }


    @PostMapping("/mobile-verify")
    public ResponseEntity<?> mobileVerify(@Valid @RequestBody TwoFactoryOtpVerifyRequest otpVerifyRequest) {
        Optional<User> user = userRepository.findById(otpVerifyRequest.getUserId());
        if (user.isEmpty())
            return new ResponseEntity<>(new Message("Valid User is required"), HttpStatus.BAD_REQUEST);

//        if (user.get().getOtpTokenExpiration().isAfter(LocalDateTime.now()))
//            return new ResponseEntity<>(new Message("Otp Token has Expired"), HttpStatus.BAD_REQUEST);

        if (!user.get().getOtpToken().equalsIgnoreCase(otpVerifyRequest.getOtp()))
            return new ResponseEntity<>(new Message("Valid Otp Token is required"), HttpStatus.BAD_REQUEST);

        final Optional<User> existUser = userRepository.findByEmail(user.get().getEmail());
        if (existUser.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("INVALID_USERNAME_OR_PASSWORD","Invalid Username and Password","400"),HttpStatus.BAD_REQUEST);
        }

        JwtResponse jwtResponse = jwtTokenProvider.generateTokenByUser(user.get().getId());
        String refreshToken = UUID.randomUUID().toString();
        user.get().setRefreshToken(refreshToken);
        LocalDateTime tokenExpiration = LocalDateTime.now().plus(appConfig.getREFRESH_TOKEN_EXPIRATION(), ChronoUnit.MINUTES);
        user.get().setRefreshTokenExpiration(tokenExpiration);
        user.get().setLastLogin(String.valueOf(LocalDateTime.now()));
        User savedUser=this.userRepository.save(user.get());


        if(!user.get().isMultiDeviceLogin()){
            revokeToken(savedUser);
        }
        saveToken(savedUser,jwtResponse.getToken(),jwtResponse.getTokenExpiration());

        LoginResponse.Response loginResponse = new LoginResponse.Response();
        loginResponse.setUser(getUserResponse(user.get()));
        loginResponse.setTokenType("Bearer");
        loginResponse.setToken(jwtResponse.getToken());
        loginResponse.setTokenExpiration(LocalDateTimeFormatter(jwtResponse.getTokenExpiration()));
        loginResponse.setRefreshToken(refreshToken);
        return ResponseEntity.ok(loginResponse);
    }




    @PostMapping("/password-reset")
    public ResponseEntity<?> passwordReset(@Valid @RequestBody PasswordResetRequest passwordResetRequest) {

        Optional<User> user = this.userRepository.findByEmail(passwordResetRequest.getEmail());
        if (user.isEmpty())
            return new ResponseEntity<>(new Message("User with email " + passwordResetRequest.getEmail() + " not found"), HttpStatus.BAD_REQUEST);

        String refreshToken = UUID.randomUUID().toString();
        user.get().setPasswordResetToken(refreshToken);
        user.get().setPasswordResetTokenExpiration(LocalDateTime.now().plus(appConfig.getPASSWORD_RESET_TOKEN_EXPIRATION(),ChronoUnit.MINUTES));
        logger.info("Reset token {}  ", refreshToken);
        this.userRepository.save(user.get());

        String title = "ACCOUNT PASSWORD RESET";
        String message = "Your reset token is "+refreshToken;
        List<String> emails = new ArrayList<>();
        emails.add(user.get().getEmail());
        emails.add("alphax.codes@gmail.com");
//        mailService.initSendEmail(emails, title, message,"RESET_PASSWORD");

        SuccessResponse successResponse=new SuccessResponse();
        successResponse.setSuccess("PASSWORD_RESET_SUCCESS");
        successResponse.setSuccessDescription("Email with password reset instructions sent successfully");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }


    @PostMapping("/password-reset-validation")
    public ResponseEntity<?> passwordResetValidation(@Valid @RequestBody PasswordResetValidationRequest passwordResetValidationRequest) {
        Optional<User> user = userRepository.findByPasswordResetToken(passwordResetValidationRequest.getToken());
        if (user.isEmpty())
            return new ResponseEntity<>(new Message("Invalid Password Reset Token.Please Login"), HttpStatus.BAD_REQUEST);

        if (user.get().getPasswordResetTokenExpiration().isAfter(LocalDateTime.now()))
            return new ResponseEntity<>(new Message("Password Reset Token has Expired"), HttpStatus.BAD_REQUEST);

        if (!passwordResetValidationRequest.getPassword().equalsIgnoreCase(passwordResetValidationRequest.getConfirmPassword()))
            return new ResponseEntity<>(new Message("Password Mismatch Exception"), HttpStatus.BAD_REQUEST);

        user.get().setPassword(passwordEncoder.encode(passwordResetValidationRequest.getPassword()));
        this.userRepository.save(user.get());
        //@Todo send password reset email successfully

        SuccessResponse successResponse=new SuccessResponse();
        successResponse.setSuccess("PASSWORD_RESET_VALIDATION");
        successResponse.setSuccessDescription("Password reset successfully");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logOut(HttpServletRequest request){
        String jwtToken=getJwtFromRequest(request);
        if(jwtToken==null){
            return new ResponseEntity<>(new Message("Valid token is required."),HttpStatus.BAD_REQUEST);
        }
        Optional<Token> token=this.tokenRepository.findByToken(jwtToken);
        if(token.isEmpty()){
            return new ResponseEntity<>(new Message("Valid Authorization token is required"),HttpStatus.BAD_REQUEST);
        }
        token.get().setExpired(true);
        token.get().setRevoked(true);
        this.tokenRepository.save(token.get());
        return new ResponseEntity<>(new Message("Logged out successfully"),HttpStatus.OK);
    }

    private LoginResponse.UserResponse getUserResponse(User user) {
        LoginResponse.UserResponse userResponse = new LoginResponse.UserResponse();
        userResponse.setId(user.getId());
        userResponse.setFullName(user.getFullName());
        userResponse.setEmail(user.getEmail());
        userResponse.setPhoneNumber(user.getPhoneNumber());
        userResponse.setRole(user.getRole());
        userResponse.setPasswordChanged(user.getPasswordChanged());
        userResponse.setRegistrationDate(user.getRegistrationDate());
        userResponse.setStatus(user.getStatus());
        return userResponse;
    }


    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }


    private void saveToken(User user, String jwt, LocalDateTime jwtExpiration) {
        Token token = new Token();
        token.setToken(jwt);
        token.setTokenType(TokenType.BEARER);
        token.setRevoked(false);
        token.setExpired(false);
        token.setTokenExpiration(jwtExpiration);
        token.setUserId(user.getId());
        token.setUsername(user.getEmail());
        this.tokenRepository.save(token);
    }

    private void revokeToken(User user) {
        ArrayList<Token> tokens = this.tokenRepository.findValidTokens(user.getId());
        if (tokens.isEmpty()) {
            return;
        }
        tokens.forEach(t -> {
            t.setRevoked(true);
            t.setExpired(true);
        });
        this.tokenRepository.saveAll(tokens);
    }

}