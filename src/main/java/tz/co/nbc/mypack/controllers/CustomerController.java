package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.models.Customer;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.CustomerRepository;
import tz.co.nbc.mypack.repositories.FingerprintRepository;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.services.OtpService;
import tz.co.nbc.mypack.utils.Constants;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/customers")
public class CustomerController {

    //Send sms about account creation

    //Random account number will be generated
    //  @Column(name = "ACCOUNT_NUMBER")
    //    private String AccountNumber;

    //Customer creation
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FingerprintRepository fingerprintRepository;

    @Autowired
    private OtpService otpService;

    @GetMapping
    public ResponseEntity<?> getCustomers() {
        return new ResponseEntity<>(this.customerRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomer(@PathVariable(value = "id") long id) {
        Optional<Customer> customer = this.customerRepository.findById(id);
        if (customer.isEmpty())
            return new ResponseEntity<>(new Message("Customer id " + id + " not found"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(customer.get(), HttpStatus.OK);
    }

    //Upload Signature
//    @PostMapping
//    @PreAuthorize("hasAuthority('AGENT') or hasAuthority('ADMIN')")
//    public ResponseEntity<?> addCustomer(UserPrincipal currentUser, @Valid @RequestBody CustomerRequest customerRequest) {
//        Optional<User> user = this.userRepository.findById(currentUser.getId());
//        if (user.isEmpty())
//            return new ResponseEntity<>(new Message("Agent id " + currentUser.getId() + " not found"), HttpStatus.BAD_REQUEST);
//        if (user.get().getRole().equalsIgnoreCase(Role.AGENT.getRole())) {
//            return new ResponseEntity<>(new Message("User provided is not an agent"), HttpStatus.BAD_REQUEST);
//        }
//
//        Customer customer = new Customer();
//        customer.setFullName(customerRequest.getFullName());
//        String phoneNumber = PhoneUtils.getMobileNumberWithCodeNoPlus(customerRequest.getPhoneNumber());
//        customer.setPhoneNumber(phoneNumber);
//        customer.setEmail(customerRequest.getEmail());
//        customer.setNinNumber(customerRequest.getNinNumber());
//        customer.setAccountType(customerRequest.getAccountType());
//        customer.setCardType(customerRequest.getCardType());
//        customer.setCitizenship(customerRequest.getCitizenship());
//        customer.setLeaderShipStatus(customerRequest.getLeaderShipStatus());
//        customer.setEmploymentStatus(customerRequest.getEmploymentStatus());
//        customer.setMaritalStatus(customer.getMaritalStatus());
//        customer.setInstitutionalName(customer.getInstitutionalName());
//        customer.setSalary(customer.getSalary());
//        customer.setStatus(CustomerStatus.NEW.name());
//        customer.setAgentId(currentUser.getId());
//        customer.setAgentName(currentUser.getFullName());
//        customer.setBranchId(user.get().getBranchId());
//        customer.setPEP(false);
//        customer.setBranchName(user.get().getBranchName());
//        Customer savedCustomer=this.customerRepository.save(customer);
//        Fingerprints rightHand=processFingerPrint(customerRequest.getRightHand(),"RIGHT",savedCustomer.getFullName(),savedCustomer.getId());
//        Fingerprints leftHand=processFingerPrint(customerRequest.getLeftHand(),"LEFT",savedCustomer.getFullName(),savedCustomer.getId());
//        this.fingerprintRepository.save(rightHand);
//        this.fingerprintRepository.save(leftHand);
//        //Send sms to customer about account creation
//        String message="Dear "+savedCustomer.getFullName()+" you have successfully opened an account at National Bank of Commerce (NBC Bank). You will receive your account number shortly.";
//        ArrayList<String> phoneNumbers=new ArrayList<>();
//        phoneNumbers.add(savedCustomer.getPhoneNumber());
//        this.otpService.sendOTPSms(phoneNumbers,message);
//        return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
//    }
//
//    public Fingerprints processFingerPrint(HandRequest handRequest,
//                                           String hand,
//                                           String customerName,
//                                           Long customerId
//    ){
//        Fingerprints fingerprint=new Fingerprints();
//        fingerprint.setHand(hand);
//        fingerprint.setIndexFinger(handRequest.getIndexFinger());
//        fingerprint.setMiddleFinger(handRequest.getMiddleFinger());
//        fingerprint.setRingFinger(handRequest.getRingFinger());
//        fingerprint.setPinkyFinger(handRequest.getPinkyFinger());
//        fingerprint.setCustomerId(customerId);
//        fingerprint.setCustomerName(customerName);
//        fingerprint.setStatus("SCANNED");
//        return fingerprint;
//    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteCustomer(@PathVariable(value = "id") long id) {
        Optional<Customer> customer = this.customerRepository.findById(id);
        if (customer.isEmpty())
            return new ResponseEntity<>(new Message("Customer id " + id + " not found"), HttpStatus.BAD_REQUEST);
        this.customerRepository.delete(customer.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
