package tz.co.nbc.mypack.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.AppStatus;
import tz.co.nbc.mypack.models.Banner;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.BannerRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.services.FileStorageService;
import tz.co.nbc.mypack.utils.Constants;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;


@Controller
@RequestMapping(Constants.API_V1 + "/banners")
public class BannerController {

    Logger logger = LogManager.getLogger(BannerController.class);

    @Autowired
    private BannerRepository bannerRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AppConfig appConfig;

    @GetMapping
    public ResponseEntity<?> getBanners(){
        return new ResponseEntity<>(this.bannerRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBanner(@PathVariable(value ="id") long id){
        Optional<Banner> banner=this.bannerRepository.findById(id);
        if(banner.isEmpty())
            return new ResponseEntity<>(new Message("Banner with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(banner.get(),HttpStatus.OK);
    }


//    @PostMapping( consumes = {"multipart/form-data"})
//    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<?> addBanner(@CurrentUser UserPrincipal currentUser, @Valid @RequestPart("banner") BannerRequest bannerRequest, @RequestPart("file") MultipartFile file){
//        fileStorageService.save(file,bannerRequest.getBannerName());
//        logger.info(bannerRequest.getBannerName());
//        Banner banner=new Banner();
//        banner.setImageUrl(bannerRequest.getBannerName());
//        banner.setImageType(bannerRequest.getBannerType());
//        banner.setStatus(GeneralStatus.ACTIVE.name());
//        banner.setCreatedBy(currentUser.getId());
//        banner.setCreatedByName(currentUser.getFullName());
//        return  new ResponseEntity<>(this.bannerRepository.save(banner),HttpStatus.CREATED);
//    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addBanner(UserPrincipal currentUser, @RequestParam(value = "file", required = false) MultipartFile file){
        Banner banner=new Banner();
        try {
            Timestamp timestamp=new Timestamp(System.currentTimeMillis());
            UUID uuid=UUID.randomUUID();
            String filename=uuid+"-"+ Objects.requireNonNull(file.getOriginalFilename()).replace(" ","-");
            fileStorageService.save(file,filename);
            String imageUrl=appConfig.getDOWNLOAD_URL()+"/banners/download/"+filename;
            banner.setName(filename);
            banner.setImageUrl(imageUrl);
            banner.setImageType(file.getContentType());
            banner.setStatus(AppStatus.ACTIVE.name());
            banner.setCreatedBy(currentUser.getId());
            banner.setCreatedByName(currentUser.getFullName());

        } catch (Exception e) {
            return new ResponseEntity<>(new Message("Failed to upload file: "+file.getOriginalFilename()),HttpStatus.BAD_REQUEST);
//            throw new BadRequestException("Failed to upload file: "+file.getOriginalFilename());
        }
        return  new ResponseEntity<>(this.bannerRepository.save(banner),HttpStatus.CREATED);
    }


    @GetMapping("/download/{filename:.+}")
    public ResponseEntity<?> getFile(@PathVariable String filename) {
        try {
            Resource file = fileStorageService.load(filename);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
        }  catch (Exception e) {
            return new ResponseEntity<>(new Message("Failed to read  file: "+filename),HttpStatus.BAD_REQUEST);
        }
    }

    //Download image by id
    @GetMapping("/download/byFileId/{id}")
    public ResponseEntity<?> getFile(@PathVariable(value ="id") long id) {
        Optional<Banner> image=this.bannerRepository.findById(id);
        if(image.isEmpty())
            return new ResponseEntity<>(new Message("Image with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        Resource file = fileStorageService.load(image.get().getName());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteBanner(@PathVariable(value ="id") long id){
        Optional<Banner> banner=this.bannerRepository.findById(id);
        if(banner.isEmpty())
            return new ResponseEntity<>(new Message("Banner with id "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.bannerRepository.delete(banner.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}