package tz.co.nbc.mypack.controllers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.cbs.nida.NidaFingerprintRequest;
import tz.co.nbc.mypack.cbsClient.CbsClient;
import tz.co.nbc.mypack.cbsClient.CbsClientRequest;
import tz.co.nbc.mypack.cbsClient.CbsClientResponse;
import tz.co.nbc.mypack.cbsClient.CbsClientUtils;
import tz.co.nbc.mypack.cbsClientRequests.*;
import tz.co.nbc.mypack.cbsClientResponses.AccountOpening.AccountOpening;
import tz.co.nbc.mypack.cbsClientResponses.authorization.Authorization;
import tz.co.nbc.mypack.cbsClientResponses.branch.Branch;
import tz.co.nbc.mypack.cbsClientResponses.nida.Nida;
import tz.co.nbc.mypack.cbsClientResponses.otp.CbsOtp;
import tz.co.nbc.mypack.cbsClientResponses.otpVerify.CbsOtpVerify;
import tz.co.nbc.mypack.cbsClientResponses.updateCustomer.UpdateCustomer;
import tz.co.nbc.mypack.config.AppConfig;
import tz.co.nbc.mypack.enums.CbsTokenType;
import tz.co.nbc.mypack.models.AppStorage;
import tz.co.nbc.mypack.models.TemporaryCustomer;
import tz.co.nbc.mypack.models.optionsData.BankBranch;
import tz.co.nbc.mypack.payloads.generic.ErrorResponse;
import tz.co.nbc.mypack.repositories.AppStorageRepository;
import tz.co.nbc.mypack.repositories.TemporaryCustomerRepository;
import tz.co.nbc.mypack.repositories.optionsData.*;
import tz.co.nbc.mypack.security.CurrentUser;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static tz.co.nbc.mypack.config.AppConfig.CBS_API_URL;


@Controller
@RequestMapping(Constants.API_V1 + "/cbs")
public class CbsController {

    @Autowired
    private TemporaryCustomerRepository temporaryCustomerRepository;

    @Autowired
    private AppConfig appConfig;

    static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Autowired
    private AppStorageRepository appStorageRepository;

    @Autowired
    private SourceOfFundsRepository sourceOfFundsRepository;

    @Autowired
    private IndustryRepository industryRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @Autowired
    private BankBranchRepository bankBranchRepository;

    @Autowired
    private BankProductsRepository bankProductsRepository;


    @Autowired
    private CbsClient cbsClient;

    @Autowired
    private CbsClientUtils cbsClientUtils;


//    private String CBS_API_URL = "https://nacbe-v001-nbc-alternative-channels.apps.ocp4-kawe.tz.af.absa.local/api/v1";
//    private String CBS_API_URL=CBS_API_URL;

    private static final Logger logger = LoggerFactory.getLogger(CbsClient.class);

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate() {
        String url = CBS_API_URL + "/Authentication/Portal/Vendors/Authenticate";
        Authorization.Request authorizationRequest = new Authorization.Request("NBCMYPACK", "NbcMyPack@2023!");
        String content = gson.toJson(authorizationRequest);

        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken("");
        cbsClientRequest.setRequireToken(false);
        cbsClientRequest.setTokenType(CbsTokenType.CBS_TOKEN.name());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            cbsClientUtils.Authenticate();
            return new ResponseEntity<>(cbsClientResponse.getContent(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/otp")
    public ResponseEntity<?> getOtp(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody CbsOtpRequest cbsOtpRequest) {

        String url = CBS_API_URL + "/Customers/Vendors/TemporaryRegister";

        CbsOtp.Request request = new CbsOtp.Request();
        request.setNin(cbsOtpRequest.getNin());
        request.setCurrentPhoneNumber(cbsOtpRequest.getPhoneNumber());
        String content = gson.toJson(request);

        Optional<AppStorage> appStorage = this.appStorageRepository.findByItemKey(CbsTokenType.CBS_TOKEN.name());
        if (appStorage.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("BEARER_TOKEN_MISSING", "Please authenticate to obtain cbs token", "400"), HttpStatus.BAD_REQUEST);
        }
        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken(appStorage.get().getItemDescription());
        cbsClientRequest.setTokenType(CbsTokenType.CBS_TOKEN.name());
        cbsClientRequest.setTokenExpiration(appStorage.get().getExpiration());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            logger.info("Response: " + cbsClientResponse.getContent());
            CbsOtp.Response response = gson.fromJson(cbsClientResponse.getContent(), CbsOtp.Response.class);
            logger.info("Response: " + response);
            UUID uuid = UUID.randomUUID();
            TemporaryCustomer temporaryCustomer = new TemporaryCustomer();
            temporaryCustomer.setCustomerId("NBC-" + uuid);
            temporaryCustomer.setPhoneNumber(cbsOtpRequest.getPhoneNumber());
            temporaryCustomer.setNinNumber(cbsOtpRequest.getNin());
            temporaryCustomer.setFirstTime(response.isFirstTime());
            temporaryCustomer.setApplicationStage("OTP_VERIFICATION");
            temporaryCustomer.setAgentName(currentUser.getFullName());
            temporaryCustomer.setAgent(currentUser.getId());
            this.temporaryCustomerRepository.save(temporaryCustomer);
            return new ResponseEntity<>(new CbsOtp.ApiResponse(temporaryCustomer.getCustomerId(), response.getResponseCode(), response.isFirstTime(), response.getMessage()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/otp-verify")
    public ResponseEntity<?> otpVerification(@CurrentUser UserPrincipal currentUser,@Valid @RequestBody CbsVerifyOtpRequest cbsVerifyOtpRequest) {

        //Find Temp customer
        Optional<TemporaryCustomer> temporaryCustomer = this.temporaryCustomerRepository.findByCustomerId(cbsVerifyOtpRequest.getCustomer());
        if (temporaryCustomer.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("TEMP_CUSTOMER_NOT_FOUND", "Temporary customer id provided not found", "400"), HttpStatus.BAD_REQUEST);
        }

        String url = "";
        if (temporaryCustomer.get().isFirstTime()) {
            url = CBS_API_URL + "/Customers/Vendors/VerifyMobileNumberForTemporaryCustomer";
        } else {
            url = CBS_API_URL + "/Customers/Vendors/VerifyMobileNumber";
        }


        CbsOtpVerify.Request cbsOtpVerify = new CbsOtpVerify.Request();
        cbsOtpVerify.setPhoneNumber(cbsVerifyOtpRequest.getPhone());
        cbsOtpVerify.setOtp(cbsVerifyOtpRequest.getOtp());
        String content = gson.toJson(cbsOtpVerify);

        Optional<AppStorage> appStorage = this.appStorageRepository.findByItemKey(CbsTokenType.CBS_TOKEN.name());
        if (appStorage.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("BEARER_TOKEN_MISSING", "Please authenticate to obtain cbs token", "400"), HttpStatus.BAD_REQUEST);
        }
        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken(appStorage.get().getItemDescription());
        cbsClientRequest.setTokenType(CbsTokenType.CBS_TOKEN.name());
        cbsClientRequest.setTokenExpiration(appStorage.get().getExpiration());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            //Save to temp customer
//            if(temporaryCustomer.get().isFirstTime()){
//                //Update nin number,phone number and token and isOtpVerified
//            }else {
//                //Update nin number,phone number,token,isOtp verified,Full name ,address,gender  etc
//            }
            CbsOtpVerify.Response response = gson.fromJson(cbsClientResponse.getContent(), CbsOtpVerify.Response.class);
            temporaryCustomer.get().setToken(response.getToken());
            temporaryCustomer.get().setTokenType(CbsTokenType.CUSTOMER_TOKEN.name());
            temporaryCustomer.get().setApplicationStage("OTP_VERIFICATION_SUCCESS");
            temporaryCustomer.get().setAgentName(currentUser.getFullName());
            temporaryCustomer.get().setAgent(currentUser.getId());
            this.temporaryCustomerRepository.save(temporaryCustomer.get());
            return new ResponseEntity<>(cbsClientResponse.getContent(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }


    //Token from otp used for nida only (0)
    @PostMapping("/nida")
    public ResponseEntity<?> nidaValidation(@CurrentUser UserPrincipal currentUser,@Valid @RequestBody Nida.Request request) {

        Optional<TemporaryCustomer> temporaryCustomer = this.temporaryCustomerRepository.findByCustomerId(request.getCustomer());
        if (temporaryCustomer.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("TEMP_CUSTOMER_NOT_FOUND", "Temporary customer id provided not found", "400"), HttpStatus.BAD_REQUEST);
        }

        String url = CBS_API_URL + "/Customers/KYCQuerying/Vendors/NIDA/Fingerprint";
        NidaFingerprintRequest nidaFingerprintRequest = new NidaFingerprintRequest(request.getFingerCode(), request.getFingerImage());
        String content = gson.toJson(nidaFingerprintRequest);
        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken(temporaryCustomer.get().getToken());
        cbsClientRequest.setTokenType(CbsTokenType.CUSTOMER_TOKEN.name());
        cbsClientRequest.setTokenExpiration(LocalDateTime.now());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            //Save full Information,nin,phone,idnumber et
            Nida.Response response=gson.fromJson(cbsClientResponse.getContent(),Nida.Response.class);
            temporaryCustomer.get().setToken(response.getToken());
            temporaryCustomer.get().setTokenType(CbsTokenType.NIDA_TOKEN.name());
            temporaryCustomer.get().setApplicationStage("NIDA_VALIDATION_SUCCESS");
            temporaryCustomer.get().setAgentName(currentUser.getFullName());
            temporaryCustomer.get().setAgent(currentUser.getId());
            this.temporaryCustomerRepository.save(temporaryCustomer.get());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }


    //Token from nida (Update Customer information) (Token Expiration 10 mins)
    @PostMapping("/update-information")
    public ResponseEntity<?> updateCustomerInformation(@CurrentUser UserPrincipal currentUser,@Valid @RequestBody CbsUpdateCustomerInformationRequest customerInformation) {

        Optional<TemporaryCustomer> temporaryCustomer = this.temporaryCustomerRepository.findByCustomerId(customerInformation.getCustomer());
        if (temporaryCustomer.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("TEMP_CUSTOMER_NOT_FOUND", "Temporary customer id provided not found", "400"), HttpStatus.BAD_REQUEST);
        }


        UpdateCustomer.Request request = new UpdateCustomer.Request();
        request.setCurrentEmail(customerInformation.getCurrentEmail());
        request.setMaritalStatus(customerInformation.getMaritalStatus());
        request.setTitle(customerInformation.getTitle());
        request.setOccupation(customerInformation.getOccupation());
        request.setJobPosition(customerInformation.getJobPosition());
        request.setEmployerName(customerInformation.getEmployerName());
        request.setEmployerAddress(customerInformation.getEmployerAddress());
        request.setMaritalStatus(customerInformation.getMaritalStatus());
        request.setEducationLevel(customerInformation.getEducationLevel());
        request.setSpouseName(customerInformation.getSpouceName());
        request.setSpoucePhoneNumber(customerInformation.getSpoucePhoneNumber());

        String url = CBS_API_URL + "/Customers/Vendors/UpdateCustomerDetails";
        String content = gson.toJson(request);

        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken(temporaryCustomer.get().getToken());
        cbsClientRequest.setTokenType(CbsTokenType.NIDA_TOKEN.name());
        cbsClientRequest.setTokenExpiration(LocalDateTime.now());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            //Return update information
            temporaryCustomer.get().setApplicationStage("UPDATE_INFORMATION_SUCCESS");
            temporaryCustomer.get().setAgentName(currentUser.getFullName());
            temporaryCustomer.get().setAgent(currentUser.getId());
            this.temporaryCustomerRepository.save(temporaryCustomer.get());
            UpdateCustomer.Response response=gson.fromJson(cbsClientResponse.getContent(), UpdateCustomer.Response.class);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }


    //Token from nida (Submit application) (Token Expiration 10 mins)
    @PostMapping("/submit-application")
    public ResponseEntity<?> submitApplication(@CurrentUser UserPrincipal currentUser,@Valid @RequestBody CbsSubmitApplicationRequest applicationRequest) {

        Optional<TemporaryCustomer> temporaryCustomer = this.temporaryCustomerRepository.findByCustomerId(applicationRequest.getCustomer());
        if (temporaryCustomer.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponse("TEMP_CUSTOMER_NOT_FOUND", "Temporary customer id provided not found", "400"), HttpStatus.BAD_REQUEST);
        }

        AccountOpening.Request request=new AccountOpening.Request();
        request.setProductId(applicationRequest.getProductId());
        request.setBranchId(applicationRequest.getBranchId());
        request.setCurrencyCode(applicationRequest.getCurrencyCode());
        request.setNeedMobileBanking(applicationRequest.getNeedMobileBanking());
        request.setNeedInternetBanking(applicationRequest.getNeedInternetBanking());
        request.setNeedHellowMoney(applicationRequest.getNeedHellowMoney());
        request.setNeedSmsAlert(applicationRequest.getNeedSmsAlert());
        request.setNeedEStatement(applicationRequest.getNeedEStatement());
        request.setNeedDebitCard(applicationRequest.getNeedDebitCard());
        request.setDescription(applicationRequest.getDescription());


        String content = gson.toJson(request);
        String url = CBS_API_URL + "/AccountOpening/Vendors/Apply";
        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent(content);
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken(temporaryCustomer.get().getToken());
        cbsClientRequest.setTokenType(CbsTokenType.CUSTOMER_TOKEN.name());
        cbsClientRequest.setTokenExpiration(LocalDateTime.now());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            //Return update information
            temporaryCustomer.get().setApplicationStage("SUBMIT_APPLICATION_SUCCESS");
            temporaryCustomer.get().setAgentName(currentUser.getFullName());
            temporaryCustomer.get().setAgent(currentUser.getId());
            this.temporaryCustomerRepository.save(temporaryCustomer.get());
            return new ResponseEntity<>(cbsClientResponse.getContent(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/sync-branches")
    public ResponseEntity<?> syncBranches() {
        String url = CBS_API_URL + "/Offices/GetOffices";

        CbsClientRequest cbsClientRequest = new CbsClientRequest();
        cbsClientRequest.setContent("{}");
        cbsClientRequest.setUrl(url);
        cbsClientRequest.setToken("");
        cbsClientRequest.setRequireToken(false);
        cbsClientRequest.setTokenType(CbsTokenType.CUSTOMER_TOKEN.name());

        CbsClientResponse cbsClientResponse = this.cbsClient.sendHttp(cbsClientRequest);
        if (cbsClientResponse.isSuccess()) {
            Type type = new TypeToken<ArrayList<Branch.Response>>() {
            }.getType();
            List<Branch.Response> branches = gson.fromJson(cbsClientResponse.getContent(), type);
            this.bankBranchRepository.deleteAll();
            List<BankBranch> bankBranchList = new ArrayList<>();

            branches.forEach(region -> {
                region.getDistricts().forEach(district -> {
                    district.getWards().forEach(ward -> {
                        ward.getOffices().forEach(office -> {
                            if (office.getType().equalsIgnoreCase("Branch")) {
                                BankBranch branch = new BankBranch();
                                branch.setName(office.getTitle());
                                branch.setBranchId(String.valueOf(office.getId()));
                                branch.setRegion(region.getName());
                                branch.setDistrict(district.getName());
                                branch.setWard(ward.getName());
                                this.bankBranchRepository.save(branch);
                                bankBranchList.add(branch);
                            }
                        });
                    });
                });
            });
            return new ResponseEntity<>(bankBranchList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(cbsClientResponse.getError(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/source-of-funds")
    public ResponseEntity<?> sourceOfFunds() {
        return new ResponseEntity<>(this.sourceOfFundsRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/industries")
    public ResponseEntity<?> industries() {
        return new ResponseEntity<>(this.industryRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/professions")
    public ResponseEntity<?> professions() {
        return new ResponseEntity<>(this.professionRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products")
    public ResponseEntity<?> products() {
        return new ResponseEntity<>(this.bankProductsRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/bank-branches")
    public ResponseEntity<?> bankBranches() {
        return new ResponseEntity<>(this.bankBranchRepository.findAll(), HttpStatus.OK);
    }
}
