package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.enums.AppStatus;
import tz.co.nbc.mypack.models.SalesDiaryTopic;
import tz.co.nbc.mypack.payloads.SalesDiaryTopicRequest;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.SalesDiaryTopicRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/diary-topics")
public class SalesDiaryTopicController {

    @Autowired
    private SalesDiaryTopicRepository salesDiaryTopicRepository;

    @GetMapping
    public ResponseEntity<?> getDiaryTopics(){
        return new ResponseEntity<>(this.salesDiaryTopicRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDiaryTopic(@PathVariable(value ="id") long id){
        Optional<SalesDiaryTopic> salesDiaryTopic=this.salesDiaryTopicRepository.findById(id);
        if(salesDiaryTopic.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary topic id "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(salesDiaryTopic.get(),HttpStatus.OK);
    }


    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addDiaryTopic(UserPrincipal currentUser, @Valid @RequestBody SalesDiaryTopicRequest salesDiaryTopicRequest){

        SalesDiaryTopic salesDiaryTopic=new SalesDiaryTopic();
        salesDiaryTopic.setName(salesDiaryTopicRequest.getName());
        salesDiaryTopic.setStatus(AppStatus.ACTIVE.name());
        salesDiaryTopic.setCreatedBy(currentUser.getId());
        salesDiaryTopic.setCreatedByName(currentUser.getFullName());
        return  new ResponseEntity<>(this.salesDiaryTopicRepository.save(salesDiaryTopic),HttpStatus.CREATED);
    }


    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> updateDiaryTopic(UserPrincipal currentUser, @Valid @RequestBody SalesDiaryTopicRequest salesDiaryTopicRequest){
        Optional<SalesDiaryTopic> salesDiaryTopic=this.salesDiaryTopicRepository.findById(salesDiaryTopicRequest.getId());
        if(salesDiaryTopic.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary topic id "+salesDiaryTopicRequest.getId()+" not found"),HttpStatus.BAD_REQUEST);
        salesDiaryTopic.get().setName(salesDiaryTopicRequest.getName());
        return  new ResponseEntity<>(this.salesDiaryTopicRepository.save(salesDiaryTopic.get()),HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteDiaryTopic(@PathVariable(value ="id") long id){
        Optional<SalesDiaryTopic> salesDiaryTopic=this.salesDiaryTopicRepository.findById(id);
        if(salesDiaryTopic.isEmpty())
            return new ResponseEntity<>(new Message("Sales diary topic id "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.salesDiaryTopicRepository.delete(salesDiaryTopic.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}