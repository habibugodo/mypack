package tz.co.nbc.mypack.controllers.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.ItemReqeust;
import tz.co.nbc.mypack.models.optionsData.Profession;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.optionsData.ProfessionRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@Controller
@RequestMapping(Constants.API_V1 + "/professions")
public class ProfessionsController {
    @Autowired
    private ProfessionRepository professionRepository;

    @GetMapping
    public ResponseEntity<?> getProfessions(){
        return new ResponseEntity<>(this.professionRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getProfession(@PathVariable(value ="id") long id){
        Optional<Profession> profession=this.professionRepository.findById(id);
        if(profession.isEmpty())
            return new ResponseEntity<>(new Message("Profession with "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(profession.get(),HttpStatus.OK);
    }


    @PostMapping("/bulk")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> getProfessionsBulk(@RequestBody List<String> stringList ){
        for (String item:stringList) {
            Profession profession=this.professionRepository.findByNameIgnoreCase(item).orElse(new Profession());
            profession.setName(item);
            this.professionRepository.save(profession);
        }
        return new ResponseEntity<>(new Message("Saved batch successfully"),HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addProfession(UserPrincipal currentUser, @Valid @RequestBody ItemReqeust itemReqeust){
        Optional<Profession> exists=this.professionRepository.findByNameIgnoreCase(itemReqeust.getName());
        if(exists.isPresent()){
            return new ResponseEntity<>(new Message("Profession "+itemReqeust.getName()+" already exist."),HttpStatus.OK);
        }
        Profession profession=new Profession();
        profession.setName(itemReqeust.getName());
        return  new ResponseEntity<>(this.professionRepository.save(profession),HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteProfession(@PathVariable(value ="id") long id){
        Optional<Profession> profession=this.professionRepository.findById(id);
        if(profession.isEmpty())
            return new ResponseEntity<>(new Message("Profession with "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.professionRepository.delete(profession.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
