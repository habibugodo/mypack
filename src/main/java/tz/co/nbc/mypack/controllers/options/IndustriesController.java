package tz.co.nbc.mypack.controllers.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.ItemReqeust;
import tz.co.nbc.mypack.models.optionsData.Industry;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.optionsData.IndustryRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@Controller
@RequestMapping(Constants.API_V1 + "/industries")
public class IndustriesController {

    @Autowired
    private IndustryRepository industryRepository;

    @GetMapping
    public ResponseEntity<?> getIndustries(){
        return new ResponseEntity<>(this.industryRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getIndustry(@PathVariable(value ="id") long id){
        Optional<Industry> industry=this.industryRepository.findById(id);
        if(industry.isEmpty())
            return new ResponseEntity<>(new Message("Industry with "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(industry.get(),HttpStatus.OK);
    }


    @PostMapping("/bulk")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addIndustriesBulk(@RequestBody List<String> stringList){
        for (String item:stringList) {
            Industry industry=this.industryRepository.findByNameIgnoreCase(item).orElse(new Industry());
            industry.setName(item);
            this.industryRepository.save(industry);
        }
        return new ResponseEntity<>(new Message("Saved batch successfully"),HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addIndustries(UserPrincipal currentUser, @Valid @RequestBody ItemReqeust itemReqeust){
        Optional<Industry> exists=this.industryRepository.findByNameIgnoreCase(itemReqeust.getName());
        if(exists.isPresent()){
            return new ResponseEntity<>(new Message("Industry with name "+itemReqeust.getName()+" already exist."),HttpStatus.OK);
        }
        Industry industry=new Industry();
        industry.setName(itemReqeust.getName());
        return  new ResponseEntity<>(this.industryRepository.save(industry),HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteIndustries(@PathVariable(value ="id") long id){
        Optional<Industry> industry=this.industryRepository.findById(id);
        if(industry.isEmpty())
            return new ResponseEntity<>(new Message("Industry with "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.industryRepository.delete(industry.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
