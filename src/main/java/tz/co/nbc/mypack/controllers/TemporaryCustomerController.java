package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import tz.co.nbc.mypack.models.TemporaryCustomer;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.TemporaryCustomerRepository;
import tz.co.nbc.mypack.utils.Constants;
import java.util.Optional;


@Controller
@RequestMapping(Constants.API_V1 + "/temporary-customer")
public class TemporaryCustomerController {

    @Autowired
    private TemporaryCustomerRepository temporaryCustomerRepository;

    @GetMapping
    public ResponseEntity<?> getTemporaryCustomers() {
        return new ResponseEntity<>(this.temporaryCustomerRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTemporaryCustomer(@PathVariable(value = "id") long id) {
        Optional<TemporaryCustomer> temporaryCustomer = this.temporaryCustomerRepository.findById(id);
        if (temporaryCustomer.isEmpty())
            return new ResponseEntity<>(new Message("Temporary customer with " + id + " not found"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(temporaryCustomer.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteCustomer(@PathVariable(value = "id") long id) {
        Optional<TemporaryCustomer> temporaryCustomer = this.temporaryCustomerRepository.findById(id);
        if (temporaryCustomer.isEmpty())
            return new ResponseEntity<>(new Message("Temporary customer with " + id + " not found"), HttpStatus.BAD_REQUEST);
        this.temporaryCustomerRepository.delete(temporaryCustomer.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }

}