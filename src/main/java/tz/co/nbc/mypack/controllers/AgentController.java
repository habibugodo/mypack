package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.enums.Role;
import tz.co.nbc.mypack.models.AccountStatus;
import tz.co.nbc.mypack.models.Branch;
import tz.co.nbc.mypack.models.User;
import tz.co.nbc.mypack.models.optionsData.BankBranch;
import tz.co.nbc.mypack.payloads.agent.AgentApprovalRequest;
import tz.co.nbc.mypack.payloads.agent.AgentRequest;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.BranchRepository;
import tz.co.nbc.mypack.repositories.CustomerRepository;
import tz.co.nbc.mypack.repositories.TemporaryCustomerRepository;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.repositories.optionsData.BankBranchRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;
import tz.co.nbc.mypack.utils.PhoneUtils;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/agents")
public class AgentController {
    //Create Agent
    //Upload nin


    //Agent creation
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private BankBranchRepository bankBranchRepository;

    @Autowired
    private TemporaryCustomerRepository temporaryCustomerRepository;

    @Autowired
    private CustomerRepository customerRepository;


    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getAgents(){
        //Order by Id desc
        return new ResponseEntity<>(this.userRepository.getAgents(Role.AGENT.getRole()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getAgent(@PathVariable(value ="id") long id){
        Optional<User> user=this.userRepository.findById(id);
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+id+" not found"),HttpStatus.BAD_REQUEST);
        if(user.get().getRole().equalsIgnoreCase(Role.AGENT.getRole())){
            return new ResponseEntity<>(user.get(),HttpStatus.OK);
        }
        return new ResponseEntity<>(new Message("Agent id "+id+" not found"),HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/{id}/customers")
//    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getAgentCustomers(@PathVariable(value ="id") long id){
        Optional<User> user=this.userRepository.findById(id);
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(this.customerRepository.getCustomerByAgent(user.get().getId()),HttpStatus.OK);
    }

    @GetMapping("/{id}/temporary-customers")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public ResponseEntity<?> getAgentTemporaryCustomers(@PathVariable(value ="id") long id){
        Optional<User> user=this.userRepository.findById(id);
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(this.temporaryCustomerRepository.getCustomerByAgent(user.get().getId()),HttpStatus.BAD_REQUEST);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addAgent(UserPrincipal currentUser, @Valid @RequestBody AgentRequest agentRequest){
        //Note email should have .nbc.co.tz prefix
        if (userRepository.existsByEmail(agentRequest.getEmail())) {
            return new ResponseEntity<>(new Message("Email is already taken"), HttpStatus.BAD_REQUEST);
        }
        //Change phone number to 255
        if (userRepository.existsByPhoneNumber(agentRequest.getPhoneNumber())) {
            return new ResponseEntity<>(new Message("Phone is already taken"), HttpStatus.BAD_REQUEST);
        }

        if(!agentRequest.getPassword().equals(agentRequest.getConfirmPassword())){
            return new ResponseEntity<>(new Message("Password mismatch"), HttpStatus.BAD_REQUEST);
        }

        Optional<BankBranch> branch=this.bankBranchRepository.findById(agentRequest.getBranchId());
        if(branch.isEmpty())
            return new ResponseEntity<>(new Message("Branch with "+agentRequest.getBranchId()+" not found"),HttpStatus.BAD_REQUEST);

        String password = passwordEncoder.encode(agentRequest.getPassword());
        User user = new User();
        user.setFullName(agentRequest.getFullName());
        user.setEmail(agentRequest.getEmail());
        user.setPassword(password);
        String phoneNumber= PhoneUtils.getMobileNumberWithCodeNoPlus(agentRequest.getPhoneNumber());
        user.setPhoneNumber(phoneNumber);
        user.setBranch(Long.parseLong(branch.get().getBranchId()));
        user.setBranchName(branch.get().getName());
        user.setStatus(AccountStatus.PENDING.name());
        user.setApprovalStatus(AccountStatus.PENDING.name());
        user.setRole(Role.AGENT.toString());
        //Send Email to Agent && Sms(AGENT_REGISTRATION)
        //Dear Hamis Juma your agent account has been created successfully.Its pending approval to be used.
        return  new ResponseEntity<>(this.userRepository.save(user),HttpStatus.CREATED);
    }

    //Approve Account
    @PutMapping("/approve")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> approveAgent(UserPrincipal currentUser, @Valid @RequestBody AgentApprovalRequest agentApprovalRequest){
        Optional<User> user=this.userRepository.findById(agentApprovalRequest.getAgentId());
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+agentApprovalRequest.getAgentId()+" not found"),HttpStatus.BAD_REQUEST);

        user.get().setStatus(AccountStatus.ACTIVE.name());
        user.get().setApprovedBy(currentUser.getId());
        user.get().setApproveByName(currentUser.getFullName());
        user.get().setApprovedRemarks(agentApprovalRequest.getRemarks());
        user.get().setApprovalStatus(AccountStatus.ACTIVE.name());
        //Send user log
        //Send Email & Sms on Agent approval
        //Click Verify to setup password
        return  new ResponseEntity<>(this.userRepository.save(user.get()),HttpStatus.OK);
    }

    //Reject Account
    @PutMapping("/reject")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> rejectAgent(UserPrincipal currentUser, @Valid @RequestBody AgentApprovalRequest agentApprovalRequest){
        Optional<User> user=this.userRepository.findById(agentApprovalRequest.getAgentId());
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("Agent id "+agentApprovalRequest.getAgentId()+" not found"),HttpStatus.BAD_REQUEST);
        user.get().setStatus(AccountStatus.REJECTED.name());
        user.get().setApprovalStatus(AccountStatus.REJECTED.name());
        user.get().setApprovedBy(currentUser.getId());
        user.get().setApproveByName(currentUser.getFullName());
        user.get().setApprovedRemarks(agentApprovalRequest.getRemarks());
        //Send user log
        //Send Email & Sms on Agent approval
        //Click Verify to setup password
        return  new ResponseEntity<>(this.userRepository.save(user.get()),HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteAgent(@PathVariable(value ="id") long id){
        Optional<User> user=this.userRepository.findById(id);
        if(user.isEmpty())
            return new ResponseEntity<>(new Message("User id "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.userRepository.delete(user.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
