package tz.co.nbc.mypack.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.User;
import tz.co.nbc.mypack.payloads.auth.AdminPasswordResetRequest;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.UserRepository;
import tz.co.nbc.mypack.utils.Constants;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping
    public ResponseEntity<?> getUsers() {
        return new ResponseEntity<>(this.userRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable(value = "id") long id) {
        Optional<User> user = this.userRepository.findById(id);
        if (user.isEmpty())
            return new ResponseEntity<>(new Message("User with " + id + " not found"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    }

    //Add User
    //Enable User
    //Disable

    //Password Reset
    @PostMapping("/admin-password-reset")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> passwordReset(@Valid @RequestBody AdminPasswordResetRequest adminPasswordResetRequest) {

        Optional<User> user = this.userRepository.findById(adminPasswordResetRequest.getUserId());
        if (user.isEmpty())
            return new ResponseEntity<>(new Message("User with " + adminPasswordResetRequest.getUserId() + " not found"), HttpStatus.BAD_REQUEST);
        if(!adminPasswordResetRequest.getPassword().equalsIgnoreCase(adminPasswordResetRequest.getConfirmPassword()))
            return new ResponseEntity<>(new Message("Password Mismatch Exception"), HttpStatus.BAD_REQUEST);
        user.get().setPassword(passwordEncoder.encode(adminPasswordResetRequest.getPassword()));
        this.userRepository.save(user.get());
        //@Todo send password reset email successfully

        return new ResponseEntity<>(new Message("Password reset successfully"),HttpStatus.OK);
    }


    //Delete User
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteAccountType(@PathVariable(value ="id") long id){
        Optional<User> user = this.userRepository.findById(id);
        if (user.isEmpty())
            return new ResponseEntity<>(new Message("User with " + id + " not found"), HttpStatus.BAD_REQUEST);
        this.userRepository.delete(user.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}