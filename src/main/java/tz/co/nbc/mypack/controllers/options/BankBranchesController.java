package tz.co.nbc.mypack.controllers.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tz.co.nbc.mypack.models.optionsData.BankBranch;
import tz.co.nbc.mypack.payloads.BankBranchRequest;
import tz.co.nbc.mypack.payloads.generic.Message;
import tz.co.nbc.mypack.repositories.optionsData.BankBranchRepository;
import tz.co.nbc.mypack.security.UserPrincipal;
import tz.co.nbc.mypack.utils.Constants;


import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(Constants.API_V1 + "/bank-branches")
public class BankBranchesController {

    @Autowired
    private BankBranchRepository bankBranchRepository;

    @GetMapping
    public ResponseEntity<?> getBankBranches(){
        return new ResponseEntity<>(this.bankBranchRepository.findAll(Sort.by(Sort.Direction.DESC, "id")), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBankBranch(@PathVariable(value ="id") long id){
        Optional<BankBranch> bankBranch=this.bankBranchRepository.findById(id);
        if(bankBranch.isEmpty())
            return new ResponseEntity<>(new Message("Bank branch with "+id+" not found"),HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(bankBranch.get(),HttpStatus.OK);
    }



    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addBankBranch(UserPrincipal currentUser, @Valid @RequestBody BankBranchRequest bankBranchRequest){
        Optional<BankBranch> exists=this.bankBranchRepository.findByNameIgnoreCase(bankBranchRequest.getName());
        if(exists.isPresent()){
            return new ResponseEntity<>(new Message("Bank branch with name  "+bankBranchRequest.getName()+" already exist."),HttpStatus.BAD_REQUEST);
        }
        BankBranch bankBranch=new BankBranch();
        bankBranch.setName(bankBranchRequest.getName());
        bankBranch.setBranchCode(bankBranch.getBranchCode());
        bankBranch.setBranchId(bankBranchRequest.getBranchId());
        return  new ResponseEntity<>(this.bankBranchRepository.save(bankBranch),HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteBankBranch(@PathVariable(value ="id") long id){
        Optional<BankBranch> bankBranch=this.bankBranchRepository.findById(id);
        if(bankBranch.isEmpty())
            return new ResponseEntity<>(new Message("Bank branches with "+id+" not found"),HttpStatus.BAD_REQUEST);
        this.bankBranchRepository.delete(bankBranch.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
